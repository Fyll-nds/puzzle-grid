# Puzzle Grid

An app for drawing puzzle grids (for crosswords, sudokus, and battleship puzzles) to avoiding having to write on papers and the like.

This app was written in Dart/Flutter by Iain Findlay (Fyll).

## Installation

Available through F-Droid: [Link](https://f-droid.org/packages/nds.fyll.puzzle_grid)

The APK can be downloaded from the Downloads tab: [Link](https://bitbucket.org/Fyll-nds/puzzle-grid/downloads/app.apk)

## How to Use

There are three types of puzzle available to choose on the front menu:

* [Crosswords](#markdown-header-crossword)

* [Sudoku](#markdown-header-sudoku)

* [Battleships](#markdown-header-battleships)

All grids also have an identical [Save/Load screen](#markdown-header-save-and-load).

### Crossword

#### Grid

![A partially filled crossword grid.](fastlane/metadata/android/en-US/images/phoneScreenshots/1.png?raw=true)

Tap black squares to turn them white and tap white squares to turn them back. Dragging sets all squares crossed to the inverse of the original square (e.g. dragging across a BWBB would change them to WWWW).

The +/- buttons along each edge add or subtract a row/column from that edge, and the dimensions are displayed above the grid.

Press the "All Black" button to fill the grid with black, "All White" to fill it with white, and "Grid" to set every 2nd square to black and the rest to white.

The symmetry drop-down can be used to enforce a symmetry onto the grid. The chosen symmetry will be applied to any cell changes _after_ the symmetry is selected.

The "Fill Grid" button switches to Fill Mode, where the numbers are visible, and turns in to an "Edit Grid" button, which switches back.

In fill mode (pictured), tapping a white cell will highlight that cell and either the row or column that it lies in (the priority goes: Start of horizontal clue; Start of vertical clue; Part of horizontal clue; Part of vertical clue), and the keyboard should open up. You can then type characters into the grid, and the selected cell will proceed to the next cell in the line as you press each key.

Tapping a black cell will close the keyboard and deselect the selected cell.

Holding the "Clear Grid" button removes all letters from the grid.

#### Clues

![The Clues screen for the crossword, with a clue highlighted.](fastlane/metadata/android/en-US/images/phoneScreenshots/2.png?raw=true)

When no clues are loaded, this will just be a button that allows you to open an image file from the device's memory.

Once an image is loaded, the "Load Image" button shrinks to the top of the screen. Tapping this again replaces the current image with the new one.

The "Enable Snips" checkbox allows you to drag a highlight over part of the image to put that image at the top of the app, so that it is visible on the Grid screen. When the image has a highlight, a new button labelled "Clear Snip" will appear that removes the highlight.

(_**Note: I would recommend that you start by dragging horizontally rather than vertically, as a vertical drag will get interpreted as an attempt to scroll.**_)

### Sudoku

![A partially filled sudoku grid.](fastlane/metadata/android/en-US/images/phoneScreenshots/3.png?raw=true)

Tap a square to bring up a number selector. Tap off of the selector to close it without changing the value, or tap a number (or the blank at the top) to change the square to that value.

Hold the "Reset Grid" button to remove all of the numbers.

Press the "Fill Grid" button to switch to Fill Mode. This turns the button into an "Edit Grid" button, which switches back.

In fill mode (pictured), the numbers entered in edit mode cannot be changed (and lack a border). Tap a square with a border to open the number selector and choose a value for that square.

If two numbers in a row/column/3x3 square are the same, they will both be highlighted in red.

The button below the grid undoes the last action.

The button at the top turns on Ghost Mode, and then splits into three buttons. In ghost mode, the numbers you enter are grey.

* The first button removes all ghost numbers and turns off ghost mode.

* The second button removes all ghost numbers _except for the first one you placed_.

* The third button converts all of the ghost numbers into real numbers, and turns off ghost mode.

Holding the "Clear Grid" button removes all of the numbers entered on the fill grid screen.

### Battleships

![A partially filled battleships grid.](fastlane/metadata/android/en-US/images/phoneScreenshots/4.png?raw=true)

Select a shape at the top and tap squares to put that shape in them. To remove a shape from a square, tap the selected shape at the top to deselect it, then tap the square you wish to empty.

Tap the numbers around the edge to open a number selector, which can be used to state how many pieces of boat are in that row/column. Tap off of the selector to close it without changing, and tap the "Type" space at the bottom to open the keyboard and type a number in.

The +/- buttons along each edge add or subtract a row/column from that edge, and the dimensions are displayed above the grid.

The app can also keep track of how many of each boat you have placed by using the "Add Boat" button. Press this to add a boat of the length given in the number selector next to the button. To remove an added boat, press the red X at the end of the boat's row. If no boats are inputted, the app will not keep track.

Hold the "Reset Grid" button to remove all of the boat parts filled in, as well as any boats added to the tracker. This does not zero the numbers.

Press the "Fill Grid" button to switch to Fill Mode. This turns the button into an "Edit Grid" button, which switches back.

Any squares filled in while in edit mode will have a grey background and be unchangeable while in fill mode. To fill boats/water into the rest of the squares, select the colour at the top (left/blue for water, right/black for boat), and then tap a square to put the boat/water into it. The grid will automatically shape the boat segments when they are capped off with water/edges.

To empty a square, deselect the buttons at the top and tap the square.

When the correct number of boat segments are in a row/column, the number at the end will turn grey. If too many boat segments are in the row/column, it will turn red. Similarly, if boats are being tracked, the boat lengths will turn grey when they are capped by the grid, and additional red numbers will be printed if too many boats of a given length are present.

The button above the grid turns on Ghost Mode, and then splits into three buttons. In ghost mode, the boat/water squares you fill are greyed.

* The first button removes all ghost squares and turns off ghost mode.

* The second button removes all ghost squares _except for the first one you placed_.

* The third button converts all of the ghost squares into real squares, and turns off ghost mode.

To undo the last action, press the undo button below the grid.

Holding the "Clear Grid" button removes all of boat/water squares filled in on the fill grid screen.

### Save and Load

![The save/load screen.](fastlane/metadata/android/en-US/images/phoneScreenshots/5.png?raw=true)

At the top of this tab, there is a text box for entering a name for the saved grid. If no name is entered, it just uses the current date (shown in grey). (_**Note: If a name is entered that is already used (or a previous puzzle has been saved on the same day), it will overwrite the previous save data.**_).

If there are any save files to load, they will be listed in a drop-down box in the second section. When one is selected, you have the choice to either load it (press the "Load" button) or delete it (press and hold the "Delete" button).

Below this, there is a checkbox that you can set to have the app auto-save and auto-load the last puzzle. Normally when you close the app it will forget everything you have entered (except anything that you have manually saved), but when this checkbox is ticked, it will instead reload whatever grid and clues were open when the app was last closed.

At the bottom, there is a button to return to the puzzle select menu.

