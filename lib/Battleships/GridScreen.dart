import "package:flutter/material.dart";

import "../Common/ToggleButton.dart";

import "GridData.dart";
import "ClueData.dart";

import "EditGrid.dart";
import "FillGrid.dart";

class GridScreenWidget extends StatefulWidget
{
    final GridData grid;
    final ClueData clues;

    GridScreenWidget (this.grid, this.clues, { Key key }) : super(key: key) {}
    GridScreenWidgetState createState() { return GridScreenWidgetState(grid, clues); }
}

class GridScreenWidgetState extends State<GridScreenWidget>
{
    GridData grid_;
    ClueData clues_;

    GridScreenWidgetState (this.grid_, this.clues_) {}

    Widget build(BuildContext ctx) {
        if(grid_.editMode())
            return EditGridWidget(grid_, clues_, toggleFunc: toggleMode);
        else
            return FillGridWidget(grid_, clues_, toggleFunc: toggleMode);
    }

    void toggleMode() { grid_.toggleMode(); setState(() {}); }
}
