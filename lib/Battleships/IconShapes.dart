import "package:flutter/material.dart";

class TriangleClipper extends CustomClipper<Path>
{
    Offset a;
    Offset b;
    Offset c;

    bool vert;

    TriangleClipper (this.a, this.b, this.c, this.vert) {}

    @override Path getClip(Size dim) {
        Path oup = Path();

        oup.moveTo(b.dx * dim.width, b.dy * dim.height);
        oup.cubicTo(
            (b.dx + (vert ? b.dx : a.dx)) * dim.width / 2.0, (b.dy + (vert ? a.dy : b.dy)) * dim.height / 2.0,
            (a.dx + (vert ? b.dx : a.dx)) * dim.width / 2.0, (a.dy + (vert ? a.dy : b.dy)) * dim.height / 2.0,
            a.dx * dim.width, a.dy * dim.height
        );
        oup.cubicTo(
            (a.dx + (vert ? c.dx : a.dx)) * dim.width / 2.0, (a.dy + (vert ? a.dy : c.dy)) * dim.height / 2.0,
            (c.dx + (vert ? c.dx : a.dx)) * dim.width / 2.0, (c.dy + (vert ? a.dy : c.dy)) * dim.height / 2.0,
            c.dx * dim.width, c.dy * dim.height
        );
        oup.close();

        /*oup.moveTo(a.dx * dim.width, a.dy * dim.height);
        oup.lineTo(b.dx * dim.width, b.dy * dim.height);
        oup.lineTo(c.dx * dim.width, c.dy * dim.height);
        oup.close();*/

        return oup;
    }

    @override bool shouldReclip(CustomClipper<Path> old) { return false; }
}

class IconShapes extends StatelessWidget
{
    static IconShapes Water = IconShapes._(0);
    static IconShapes Single = IconShapes._(1);
    static IconShapes Middle = IconShapes._(2);
    static IconShapes End_U = IconShapes._(3);
    static IconShapes End_R = IconShapes._(4);
    static IconShapes End_D = IconShapes._(5);
    static IconShapes End_L = IconShapes._(6);

    final int index;

    IconShapes._(this.index) {}

    Widget build(BuildContext ctx) {
        if(index == 0)
            return Container(color: Colors.blue);
        else if(index == 1)
            return Container(decoration: BoxDecoration(shape: BoxShape.circle, color: Colors.black));
        else if(index == 2)
            return Container(color: Colors.black);
        else if(index == 3)
            return ClipPath(clipper: TriangleClipper(Offset(0.5, 0.0), Offset(0.0, 1.0), Offset(1.0, 1.0), true), child: Container(color: Colors.black));
        else if(index == 4)
            return ClipPath(clipper: TriangleClipper(Offset(1.0, 0.5), Offset(0.0, 0.0), Offset(0.0, 1.0), false), child: Container(color: Colors.black));
        else if(index == 5)
            return ClipPath(clipper: TriangleClipper(Offset(0.5, 1.0), Offset(0.0, 0.0), Offset(1.0, 0.0), true), child: Container(color: Colors.black));
        else
            return ClipPath(clipper: TriangleClipper(Offset(0.0, 0.5), Offset(1.0, 0.0), Offset(1.0, 1.0), false), child: Container(color: Colors.black));
    }
}