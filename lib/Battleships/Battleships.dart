import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/PuzzleWidgetState.dart";
import "../Common/SaveLoad.dart";
import "../Common/AutoSaveLoad.dart";
import "../Common/SaveLoadScreen.dart";

import "BattleshipsData.dart";

import "GridScreen.dart";

class BattleshipsWidget extends StatefulWidget
{
    final BattleshipsData data;

    BattleshipsWidget (this.data, { Key key }) : super(key: key) {}
    BattleshipsWidgetState createState() { return BattleshipsWidgetState(data); }
}

class BattleshipsWidgetState extends PuzzleWidgetState<BattleshipsWidget>
{
    BattleshipsData data_;

    BattleshipsWidgetState (this.data_) {
        initTabs([ "Grid" ]);

        AutoSaveLoad.setSaveFunc(Puzzle.Battleships, save);
        AutoSaveLoad.load(Puzzle.Battleships, loadFromData, () { setState(() {}); });
    }

    void onTabChange() { data_.grid.clearGhosts(); }

    void loadFromData(Map<String, dynamic> data) {
        data_.grid.loadFromData(
            data["width"], data["height"], data["ghostMode"],
            List<int>.from(data["vals"]), List<bool>.from(data["ghosts"]), List<bool>.from(data["fixeds"]), List<int>.from(data["undos"])
        );
        data_.clues.loadFromData(data["width"], data["height"], List<int>.from(data["hozClues"]), List<int>.from(data["vertClues"]), List<int>.from(data["boats"]));
    }

    void save(String fileName, String key) {
        SaveLoad.save(fileName, key, Map<String, dynamic>.fromEntries([
            MapEntry<String, dynamic>("width", data_.grid.w()), MapEntry<String, dynamic>("height", data_.grid.h()),
            MapEntry<String, dynamic>("ghostMode", data_.grid.ghostMode_),
            MapEntry<String, dynamic>("vals", data_.grid.listValues()),
            MapEntry<String, dynamic>("ghosts", data_.grid.listGhosts()),
            MapEntry<String, dynamic>("fixeds", data_.grid.listFixeds()),
            MapEntry<String, dynamic>("undos", data_.grid.listUndos()),
            MapEntry<String, dynamic>("hozClues", data_.clues.hozClues_),
            MapEntry<String, dynamic>("vertClues", data_.clues.vertClues_),
            MapEntry<String, dynamic>("boats", data_.clues.boats_)
        ]));
    }

    List<Widget> tabWidgets() {
        return [
            GridScreenWidget(data_.grid, data_.clues),
            SaveLoadScreenWidget(saveFunc: save, loadFunc: loadFromData, fileName: BATTLESHIPS_SAVE_FILE_NAME)
        ];
    }
}
