import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/GhostButton.dart";
import "../Common/ToggleButton.dart";

import "GridData.dart";
import "ClueData.dart";
import "IconShapes.dart";

class FillGridWidget extends StatefulWidget
{
    final GridData grid;
    final ClueData clues;

    final void Function() toggleFunc;

    FillGridWidget (this.grid, this.clues, { Key key, this.toggleFunc = null }) : super(key: key) {}
    FillGridWidgetState createState() { return FillGridWidgetState(grid, clues, toggleFunc); }
}

class FillGridWidgetState extends State<FillGridWidget>
{
    GridData grid_;
    ClueData clues_;

    void Function() toggleFunc;

    bool fillingWater_ = null;
    int xLast_ = -1;
    int yLast_ = -1;

    GlobalKey gdKey_ = GlobalKey();

    GlobalKey ghostKey_ = GlobalKey();

    FillGridWidgetState (this.grid_, this.clues_, this.toggleFunc) {}

    @override Widget build(BuildContext ctx) {
        return ListView(children: [
            Row(children: [
                boatButton(ctx, IconShapes.Water, true, 0.5),
                boatButton(ctx, IconShapes.Middle, false, 0.5)
            ]),
            GhostButtonWidget(
                1.0,
                key: ghostKey_,
                startPressed: grid_.ghostMode_,
                onPress: () { grid_.ghostMode_ = true; setState(() {}); },
                onClear: (bool allButFirst) { grid_.clearGhosts(removeFirst: !allButFirst); setState(() {}); },
                onKeep: () { grid_.keepGhosts(); setState(() {}); },
                iconFill: (int index) { return [ IconShapes.End_L, IconShapes.Middle, IconShapes.End_R ][index]; }
            ),
            Row(children: [
                Expanded(child: Container()),
                GestureDetector(
                    key: gdKey_,
                    child: grid_.build(gridWidth(ctx), gridHeight(ctx), false, clues_, null),
                    onTapDown: (TapDownDetails details) { press(details.localPosition); },
                    onTapUp: (TapUpDetails details) { release(); },
                    onPanUpdate: (DragUpdateDetails details) { press(details.localPosition); },
                    onPanEnd: (DragEndDetails details) { release(); },
                    onVerticalDragUpdate: (DragUpdateDetails details) { press(details.localPosition); },
                    onVerticalDragEnd: (DragEndDetails details) { release(); }
                )
            ]),
            Divider(),
            clues_.printBoats(grid_.completeBoats()),
            ((clues_.boats_.length > 0) ? Divider() : Container()),
            ToggleButtonWidget(Container(width: 32.0, height: 32.0, child: Icon(Icons.undo)), 1.0, onPress: undo),
            Row(children: [
                ToggleButtonWidget(Text("Clear Grid (hold)"), 0.5, h: 48.0, onLongPress: clearGrid),
                ToggleButtonWidget(Text("Edit Grid"), 0.5, h: 48.0, onPress: toggleFunc)
            ])
        ]);
    }

    Widget boatButton(BuildContext ctx, Widget lbl, bool setToVal, double w) {
        return ToggleButtonWidget(
            Container(width: 32.0, height: 32.0, child: lbl), w,
            isPressed: () { return (fillingWater_ != null && fillingWater_ == setToVal); },
            onPress: () { setState(() { fillingWater_ = ((fillingWater_ != null && fillingWater_ == setToVal) ? null : setToVal); }); }
        );
    }

    void press(Offset pos) {
        int xThis = pos.dx ~/ grid_.cellWidth();
        int yThis = pos.dy ~/ grid_.cellHeight();

        if((xThis != xLast_ || yThis != yLast_) && !grid_.isFixed(xThis, yThis) && (!grid_.ghostMode_ || grid_.at(xThis, yThis) == GridValue.Empty || grid_.isGhost(xThis, yThis))) {
            if(fillingWater_ == null && grid_.at(xThis, yThis) != GridValue.Empty)
                grid_.setValInCell(xThis, yThis, GridValue.Empty, false, true);
            else if(fillingWater_ != null && fillingWater_ && grid_.at(xThis, yThis) != GridValue.Water)
                grid_.setValInCell(xThis, yThis, GridValue.Water, false, true);
            else if(fillingWater_ != null && !fillingWater_ && grid_.at(xThis, yThis) != GridValue.Boat)
                grid_.setValInCell(xThis, yThis, GridValue.Boat, false, true);

            xLast_ = xThis;
            yLast_ = yThis;

            AutoSaveLoad.save(Puzzle.Battleships);
            setState(() {});
        }
    }

    void release() { xLast_ = -1; yLast_ = -1; }

    void undo() {
        grid_.undo();
        AutoSaveLoad.save(Puzzle.Battleships);

        setState(() {});
    }

    void clearGrid() {
        grid_.clear(false);
        AutoSaveLoad.save(Puzzle.Battleships);

        setState(() {});
    }

    double gridWidth(BuildContext ctx) { return MediaQuery.of(ctx).size.width - 4.0; }
    double gridHeight(BuildContext ctx) { return gridWidth(ctx) * grid_.h() / grid_.w(); }
}
