import "package:flutter/material.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/NumberSelect.dart";

import "../Puzzles.dart";

class ClueData
{
    List<int> hozClues_ = [];
    List<int> vertClues_ = [];

    List<GlobalKey> hozKeys_ = [];
    List<GlobalKey> vertKeys_ = [];

    List<int> boats_ = [];

    ClueData (int w, int h) { reset(w, h); }

    Widget printBoats(List<int> completeBoats, { void Function(int) onSubButtonPress = null }) {
        List< List<Widget> > rows = [[ Expanded(child: Container()) ]];

        for(int i = 0; i < boats_.length; ++i) {
            if(i > 0 && boats_[i] != boats_[i - 1]) {
                if(onSubButtonPress != null) {
                    rows.last.add(Text(" -- ", style: TextStyle(color: Colors.grey)));
                    rows.last.add(GestureDetector(child: Text("X", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)), onTap: () { onSubButtonPress(boats_[i - 1]); }));
                }

                rows.last.add(Expanded(child: Container()));
                rows.add([ Expanded(child: Container()) ]);
            }

            if(completeBoats.remove(boats_[i]))
                rows.last.add(Text(boats_[i].toString(), style: TextStyle(color: Colors.grey)));
            else
                rows.last.add(Text(boats_[i].toString(), style: TextStyle(fontWeight: FontWeight.bold)));

            //Don't put a dash at the end of the line.
            if(i < boats_.length - 1 && boats_[i] == boats_[i + 1])
                rows.last.add(Text(" - ", style: TextStyle(color: Colors.grey)));
            //Print any extra boats on the end in red.
            else {
                while(completeBoats.remove(boats_[i])) {
                    rows.last.add(Text(" - ", style: TextStyle(color: Colors.grey)));
                    rows.last.add(Text(boats_[i].toString(), style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)));
                }
            }
        }

        if(boats_.length > 0 && onSubButtonPress != null) {
            rows.last.add(Text(" -- ", style: TextStyle(color: Colors.grey)));
            rows.last.add(GestureDetector(child: Text("X", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.red)), onTap: () { onSubButtonPress(boats_.last); }));
        }

        rows.last.add(Expanded(child: Container()));
        return Column(children: List<Widget>.generate(rows.length, (int y) { return Row(children: rows[y]); }));
    }

    Widget buildHozClue(double Function() widthFunc, int index, int nBoats, bool editScreen, void Function() setState) {
        if(editScreen) {
            return NumberSelectWidget(
                hozClues_[index],
                (int newVal) {
                    hozClues_[index] = newVal;
                    if(setState != null)
                        setState();

                    AutoSaveLoad.save(Puzzle.Battleships);
                },
                key: hozKeys_[index],
                width: widthFunc, height: widthFunc
            );
        }
        else {
            return Container(
                width: widthFunc(), height: widthFunc(),
                child: Align(child: Text(hozClues_[index].toString(), style: TextStyle(
                    fontWeight: ((nBoats == hozClues_[index]) ? FontWeight.normal : FontWeight.bold),
                    color: ((nBoats == hozClues_[index]) ? Colors.grey : ((nBoats < hozClues_[index]) ? Colors.black : Colors.red))
                )))
            );
        }
    }

    Widget buildVertClue(double Function() widthFunc, int index, int nBoats, bool editScreen, void Function() setState) {
        if(editScreen) {
            return NumberSelectWidget(
                vertClues_[index],
                (int newVal) {
                    vertClues_[index] = newVal;
                    if(setState != null)
                        setState();

                    AutoSaveLoad.save(Puzzle.Battleships);
                },
                key: vertKeys_[index],
                width: widthFunc, height: widthFunc
            );
        }
        else {
            return Container(
                width: widthFunc(), height: widthFunc(),
                child: Align(child: Text(vertClues_[index].toString(), style: TextStyle(
                    fontWeight: ((nBoats == vertClues_[index]) ? FontWeight.normal : FontWeight.bold),
                    color: ((nBoats == vertClues_[index]) ? Colors.grey : ((nBoats < vertClues_[index]) ? Colors.black : Colors.red))
                )))
            );
        }
    }

    void addBoat(int boatLength) {
        boats_.add(boatLength);
        boats_.sort((int a, int b) { return b - a; });

        AutoSaveLoad.save(Puzzle.Battleships);
    }

    void subBoat(int boatLength) {
        boats_.remove(boatLength);
        AutoSaveLoad.save(Puzzle.Battleships);
    }

    void reset(int w, int h) {
        hozClues_ = List<int>.filled(w, 0, growable: true);
        vertClues_ = List<int>.filled(h, 0, growable: true);
        hozKeys_ = List<GlobalKey>.generate(w, (_) { return GlobalKey(); });
        vertKeys_ = List<GlobalKey>.generate(h, (_) { return GlobalKey(); });

        boats_ = [];

        AutoSaveLoad.save(Puzzle.Battleships);
    }

    //When you add at the top or left, all of the widgets need forcefully rebuilding or else they point to the wrong clue index.
    void addRowTop() { vertClues_.insert(0, 0); vertKeys_ = List<GlobalKey>.generate(vertClues_.length, (_) { return GlobalKey(); }); }
    void subRowTop() { vertClues_.removeAt(0); vertKeys_ = List<GlobalKey>.generate(vertClues_.length, (_) { return GlobalKey(); }); }
    void addRowBot() { vertClues_.add(0); vertKeys_.add(GlobalKey()); }
    void subRowBot() { vertClues_.removeAt(vertClues_.length - 1); vertKeys_.removeAt(vertKeys_.length - 1); }

    void addColLeft() { hozClues_.insert(0, 0); hozKeys_ = List<GlobalKey>.generate(hozClues_.length, (_) { return GlobalKey(); }); }
    void subColLeft() { hozClues_.removeAt(0); hozKeys_ = List<GlobalKey>.generate(hozClues_.length, (_) { return GlobalKey(); }); }
    void addColRight() { hozClues_.add(0); hozKeys_.add(GlobalKey()); }
    void subColRight() { hozClues_.removeAt(hozClues_.length - 1); hozKeys_.removeAt(hozKeys_.length - 1); }

    //For saving/loading.
    void loadFromData(int w, int h, List<int> hozClues, List<int> vertClues, List<int> boats) {
        hozClues_ = List<int>.generate(w, (int index) { return hozClues[index]; });
        vertClues_ = List<int>.generate(h, (int index) { return vertClues[index]; });
        hozKeys_ = List<GlobalKey>.generate(w, (_) { return GlobalKey(); });
        vertKeys_ = List<GlobalKey>.generate(h, (_) { return GlobalKey(); });

        boats_ = List<int>.generate(boats.length, (int index) { return boats[index]; });
    }
}
