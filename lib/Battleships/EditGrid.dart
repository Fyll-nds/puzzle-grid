import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/NumberSelect.dart";
import "../Common/ToggleButton.dart";

import "GridData.dart";
import "ClueData.dart";
import "IconShapes.dart";

double SIZE_BTN_WIDTH = 32.0;

class EditGridWidget extends StatefulWidget
{
    final GridData grid;
    final ClueData clues;

    final void Function() toggleFunc;

    EditGridWidget (this.grid, this.clues, { Key key, this.toggleFunc = null }) : super(key: key) {}
    EditGridWidgetState createState() { return EditGridWidgetState(grid, clues, toggleFunc); }
}

class EditGridWidgetState extends State<EditGridWidget>
{
    GridData grid_;
    ClueData clues_;

    void Function() toggleFunc;

    GridValue fillValue_ = GridValue.Empty;

    int newBoatValue_ = 1;
    GlobalKey newBoatKey_ = GlobalKey();

    EditGridWidgetState (this.grid_, this.clues_, this.toggleFunc) {}

    Widget build(BuildContext ctx) {
        return ListView(children: [
            Center(child: Text("Dimensions: " + grid_.w().toString() + " x " + grid_.h().toString())),
            Row(children: [
                boatButton(ctx, IconShapes.Water, GridValue.Water, 1.0 / 3.0),
                boatButton(ctx, IconShapes.Single, GridValue.Boat_Single, 1.0 / 3.0),
                boatButton(ctx, IconShapes.Middle, GridValue.Boat_Middle, 1.0 / 3.0)
            ]),
            Row(children: [
                boatButton(ctx, IconShapes.End_U, GridValue.Boat_End_U, 0.25),
                boatButton(ctx, IconShapes.End_D, GridValue.Boat_End_D, 0.25),
                boatButton(ctx, IconShapes.End_R, GridValue.Boat_End_R, 0.25),
                boatButton(ctx, IconShapes.End_L, GridValue.Boat_End_L, 0.25)
            ]),
            Divider(),
            vPlus(ctx, true),
            Row(children: [
                hPlus(ctx, true),
                Expanded(child: Container()),
                GestureDetector(
                    child: grid_.build(gridWidth(ctx), gridHeight(ctx), true, clues_, () { setState(() {}); }),
                    onTapDown: onTap
                ),
                Expanded(child: Container()),
                hPlus(ctx, false)
            ]),
            vPlus(ctx, false),
            Divider(),
            Row(children: [
                Container(
                    width: MediaQuery.of(ctx).size.width / 2.0,
                    child: Row(children: [
                        Expanded(child: Container()),
                        NumberSelectWidget(
                            1, (int newVal) { setState(() { newBoatValue_ = newVal; }); },
                            key: newBoatKey_,
                            width: () { return grid_.cellWidth(); }, height: () { return grid_.cellHeight(); },
                            withZero: false
                        ),
                        ToggleButtonWidget(Text("Add Boat"), 0.25, h: 48.0, onPress: () { clues_.addBoat(newBoatValue_); setState(() {}); }),
                        Expanded(child: Container())
                    ])
                ),
                Container(
                    width: MediaQuery.of(ctx).size.width / 2.0,
                    child: clues_.printBoats([], onSubButtonPress: (int len) { clues_.subBoat(len); setState(() {}); })
                )
            ]),
            Divider(),
            Row(children: [
                ToggleButtonWidget(Text("Reset Grid (hold)"), 0.5, h: 48.0, onLongPress: () { grid_.clear(true); clues_.reset(grid_.w(), grid_.h()); setState(() {}); }),
                ToggleButtonWidget(Text("Fill Grid"), 0.5, h: 48.0, onPress: toggleFunc)
            ])
        ]);
    }

    Widget boatButton(BuildContext ctx, IconShapes icon, GridValue val, double w) {
        return ToggleButtonWidget(
            Container(width: 32.0, height: 32.0, child: icon), w,
            isPressed: () { return (fillValue_ == val); },
            onPress: () { setState(() { fillValue_ = ((fillValue_ == val) ? GridValue.Empty : val); }); }
        );
    }

    Widget vPlus(BuildContext ctx, bool top) {
        return Row(children: [
            Container(width: SIZE_BTN_WIDTH),
            editButton(ctx, gridWidth(ctx) / 2.0, SIZE_BTN_WIDTH, "+", (top ? addRowTop : addRowBot)),
            editButton(ctx, gridWidth(ctx) / 2.0, SIZE_BTN_WIDTH, "-", (top ? subRowTop : subRowBot)),
            Container(width: SIZE_BTN_WIDTH)
        ]);
    }

    Widget hPlus(BuildContext ctx, bool left) {
        return Column(children: [
            editButton(ctx, SIZE_BTN_WIDTH, gridHeight(ctx) / 2.0, "+", (left ? addColLeft : addColRight)),
            editButton(ctx, SIZE_BTN_WIDTH, gridHeight(ctx) / 2.0, "-", (left ? subColLeft : subColRight))
        ]);
    }

    void addRowTop() { grid_.addRowTop(); clues_.addRowTop(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }
    void subRowTop() { grid_.subRowTop(); clues_.subRowTop(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }
    void addRowBot() { grid_.addRowBot(); clues_.addRowBot(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }
    void subRowBot() { grid_.subRowBot(); clues_.subRowBot(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }

    void addColLeft() { grid_.addColLeft(); clues_.addColLeft(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }
    void subColLeft() { grid_.subColLeft(); clues_.subColLeft(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }
    void addColRight() { grid_.addColRight(); clues_.addColRight(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }
    void subColRight() { grid_.subColRight(); clues_.subColRight(); setState(() {}); AutoSaveLoad.save(Puzzle.Battleships); }

    Widget editButton(BuildContext ctx, double w, double h, String txt, void Function() func) {
        return ToggleButtonWidget(Text(txt), w / MediaQuery.of(ctx).size.width, h: h, onPress: func);
    }

    void onTap(TapDownDetails details) {
        int xTap = details.localPosition.dx ~/ grid_.cellWidth();
        int yTap = details.localPosition.dy ~/ grid_.cellHeight();

        if(grid_.at(xTap, yTap) != fillValue_) {
            grid_.setValInCell(xTap, yTap, fillValue_, (fillValue_ != GridValue.Empty), false);

            AutoSaveLoad.save(Puzzle.Battleships);
            setState(() {});
        }
    }

    double gridWidth(BuildContext ctx) { return MediaQuery.of(ctx).size.width - (2.0 * SIZE_BTN_WIDTH); }
    double gridHeight(BuildContext ctx) { return gridWidth(ctx); }
}
