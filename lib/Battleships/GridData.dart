import "package:flutter/material.dart";

import "IconShapes.dart";
import "ClueData.dart";

enum GridValue { Empty, Water, Boat, Boat_Single, Boat_Middle, Boat_End_U, Boat_End_R, Boat_End_D, Boat_End_L }
class CellData {
    GridValue val; bool ghost; bool fixed;
    CellData (this.val, this.ghost, this.fixed) {}
    bool get isBoat { return (val != GridValue.Empty && val != GridValue.Water); }
}
class UndoData { int x; int y; GridValue oldVal; bool wasGhost; UndoData (this.x, this.y, this.oldVal, this.wasGhost) {} }

class GridData
{
    int w_ = 0;
    int h_ = 0;
    double cellWidth_ = 0.0;

    List< List<CellData> > cells_ = [];

    bool editMode_ = true;
    bool ghostMode_ = false;

    List<UndoData> undos_ = [];

    GridData (this.w_, this.h_) {
        cells_ = [];

        for(int j = 0; j < h_; ++j) {
            cells_.add([]);
            for(int i = 0; i < w_; ++i)
                cells_[j].add(CellData(GridValue.Empty, false, false));
        }
    }

    Widget build(double width, double height, bool editScreen, ClueData clues, void Function() setState) {
        cellWidth_ = width / (w_ + 1);
        if(height / (h_ + 1) < cellWidth_)
            cellWidth_ = height / (h_ + 1);

        return Column(children: List<Widget>.generate(h_ + 1, (int y) { return Row(children: List<Widget>.generate(w_ + 1, (int x) {
            if(x == w_ && y == h_)
                return Container(width: cellWidth_, height: cellWidth_);
            else if(x == w_)
                return clues.buildVertClue(() { return cellWidth_; }, y, nBoatsInRow(y), editScreen, setState);
            else if(y == h_)
                return clues.buildHozClue(() { return cellWidth_; }, x, nBoatsInCol(x), editScreen, setState);
            else
                return genCellWidget(x, y, editScreen);
        })); }));
    }

    Widget genCellWidget(int x, int y, bool onlyShowFixed) {
        Widget child;

        if(cells_[y][x].val == GridValue.Empty || (onlyShowFixed && !cells_[y][x].fixed))
            child = null;
        else if(cells_[y][x].val == GridValue.Water)
            child = IconShapes.Water;
        else {
            GridValue val = cells_[y][x].val;
            //If you're just a generic boat tile, decide what you look like.
            if(val == GridValue.Boat) {
                List<bool> isWater = [ at(x, y - 1) == GridValue.Water, at(x + 1, y) == GridValue.Water, at(x, y + 1) == GridValue.Water, at(x - 1, y) == GridValue.Water ];
                List<bool> isBoat = [
                    y > 0 && cells_[y - 1][x].isBoat, x < w_ - 1 && cells_[y][x + 1].isBoat,
                    y < h_ - 1 && cells_[y + 1][x].isBoat, x > 0 && cells_[y][x - 1].isBoat
                ];

                if(isWater[0] && isWater[1] && isWater[2] && isWater[3])
                    val = GridValue.Boat_Single;
                else if(isWater[0] && ((isWater[1] && isWater[3]) || (!isBoat[1] && isBoat[2] && !isBoat[3])))
                    val = GridValue.Boat_End_U;
                else if(isWater[1] && ((isWater[0] && isWater[2]) || (!isBoat[0] && !isBoat[2] && isBoat[3])))
                    val = GridValue.Boat_End_R;
                else if(isWater[2] && ((isWater[1] && isWater[3]) || (isBoat[0] && !isBoat[1] && !isBoat[3])))
                    val = GridValue.Boat_End_D;
                else if(isWater[3] && ((isWater[0] && isWater[2]) || (!isBoat[0] && isBoat[1] && !isBoat[2])))
                    val = GridValue.Boat_End_L;
                else
                    val = GridValue.Boat_Middle;
            }

            if(val == GridValue.Boat_Single)
                child = IconShapes.Single;
            else if(val == GridValue.Boat_Middle)
                child = IconShapes.Middle;
            else if(val == GridValue.Boat_End_U)
                child = IconShapes.End_U;
            else if(val == GridValue.Boat_End_R)
                child = IconShapes.End_R;
            else if(val == GridValue.Boat_End_D)
                child = IconShapes.End_D;
            else
                child = IconShapes.End_L;
        }

        Widget oup = Container(
            key: GlobalKey(),
            width: cellWidth_, height: cellWidth_,
            decoration: BoxDecoration(color: (cells_[y][x].fixed ? Color(0xffc0c0c0) : Colors.white), border: Border.all(width: 1.0, color: Colors.black)),
            padding: EdgeInsets.all(2.0),
            child: child
        );

        if(cells_[y][x].ghost)
            return Stack(children: [ oup, Container(width: cellWidth_, height: cellWidth_, color: Color(0x80ffffff)) ]);
        else
            return oup;
    }

    void toggleMode() {
        if(editMode_)
            startFill();
        else {
            clearGhosts();
            startEdit();
        }
    }

    void startFill() { editMode_ = false; }
    void startEdit() { editMode_ = true; }
    bool editMode() { return editMode_; }

    void clear(bool incFixed) {
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                if(incFixed || !cells_[j][i].fixed)
                    cells_[j][i] = CellData(GridValue.Empty, false, false);

        undos_ = [];
    }

    void clearGhosts({ bool removeFirst = true }) {
        int firstGhostX = -1;
        int firstGhostY = -1;
        while(undos_.isNotEmpty && isGhost(undos_.last.x, undos_.last.y)) {
            if(removeFirst || (undos_.length > 1 && isGhost(undos_[undos_.length - 2].x, undos_[undos_.length - 2].y)))
                undos_.removeAt(undos_.length - 1);
            else {
                firstGhostX = undos_.last.x;
                firstGhostY = undos_.last.y;
                break;
            }
        }

        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                if(cells_[j][i].ghost && (removeFirst || i != firstGhostX || j != firstGhostY))
                    cells_[j][i] = CellData(GridValue.Empty, false, false);

        ghostMode_ = !removeFirst;
    }

    void keepGhosts() {
        for(int i = 0; i < undos_.length; ++i)
            undos_[i].wasGhost = false;

        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                cells_[j][i].ghost = false;

        ghostMode_ = false;
    }

    void undo() {
        if(undos_.isNotEmpty) {
            cells_[undos_.last.y][undos_.last.x].val = undos_.last.oldVal;
            cells_[undos_.last.y][undos_.last.x].ghost = undos_.last.wasGhost;

            undos_.removeAt(undos_.length - 1);
        }
    }

    void setValInCell(int x, int y, GridValue newVal, bool fixed, bool withUndo) {
        if(x >= 0 && y >= 0 && x < w_ && y < h_) {
            if(withUndo)
                undos_.add(UndoData(x, y, cells_[y][x].val, cells_[y][x].ghost));

            cells_[y][x].val = newVal;
            cells_[y][x].fixed = fixed;
            cells_[y][x].ghost = ghostMode_;
        }
    }

    int w() { return w_; }
    int h() { return h_; }
    double cellWidth() { return cellWidth_; }
    double cellHeight() { return cellWidth_; }

    GridValue at(int x, int y) { if(x >= 0 && x < cells_[0].length && y >= 0 && y < cells_.length) { return cells_[y][x].val; } else { return GridValue.Water; } }
    bool isFixed(int x, int y) { if(x >= 0 && x < cells_[0].length && y >= 0 && y < cells_.length) { return cells_[y][x].fixed; } else { return true; } }
    bool isGhost(int x, int y) { if(x >= 0 && x < cells_[0].length && y >= 0 && y < cells_.length) { return cells_[y][x].ghost; } else { return false; } }

    int nBoatsInRow(int index) {
        int oup = 0;
        for(int i = 0; i < w_; ++i)
            if(cells_[index][i].isBoat)
                ++oup;

        return oup;
    }
    int nBoatsInCol(int index) {
        int oup = 0;
        for(int i = 0; i < h_; ++i)
            if(cells_[i][index].isBoat)
                ++oup;

        return oup;
    }

    List<int> completeBoats() {
        List<int> oup = [];

        for(int j = 0; j < h_; ++j) {
            for(int i = 0; i < w_; ++i) {
                if(cells_[j][i].isBoat) {
                    //Check top-cap.
                    if(j < h_ - 1 && (at(i, j - 1) == GridValue.Water || cells_[j][i].val == GridValue.Boat_End_U) && cells_[j + 1][i].isBoat) {
                        int length = 1;
                        for(; j + length < h_ && cells_[j + length][i].isBoat; ++length) {}

                        //If the boat is properly capped, then make a note of it.
                        if(at(i, j + length) == GridValue.Water || at(i, j + length - 1) == GridValue.Boat_End_D)
                            oup.add(length);
                    }
                    //Check left-cap.
                    else if(i < w_ - 1 && (at(i - 1, j) == GridValue.Water || cells_[j][i].val == GridValue.Boat_End_L) && cells_[j][i + 1].isBoat) {
                        int length = 1;
                        for(; i + length < w_ && cells_[j][i + length].isBoat; ++length) {}

                        if(at(i + length, j) == GridValue.Water || at(i + length - 1,j) == GridValue.Boat_End_R)
                            oup.add(length);
                    }
                    //Check single boat.
                    else if(cells_[j][i].val == GridValue.Boat_Single || (at(i, j - 1) == GridValue.Water && at(i + 1, j) == GridValue.Water && at(i, j + 1) == GridValue.Water && at(i - 1, j) == GridValue.Water))
                        oup.add(1);
                }
            }
        }

        return oup;
    }

    void addRowTop() { ++h_; cells_.insert(0, []); for(int i = 0; i < w_; ++i) { cells_[0].add(CellData(GridValue.Empty, false, false)); } }
    void subRowTop() { --h_; cells_.removeAt(0); }
    void addRowBot() { ++h_; cells_.add([]); for(int i = 0; i < w_; ++i) { cells_.last.add(CellData(GridValue.Empty, false, false)); } }
    void subRowBot() { --h_; cells_.removeAt(cells_.length - 1); }

    void addColLeft() { ++w_; for(int i = 0; i < h_; ++i) { cells_[i].insert(0, CellData(GridValue.Empty, false, false)); } }
    void subColLeft() { --w_; for(int i = 0; i < h_; ++i) { cells_[i].removeAt(0); } }
    void addColRight() { ++w_; for(int i = 0; i < h_; ++i) { cells_[i].add(CellData(GridValue.Empty, false, false)); } }
    void subColRight() { --w_; for(int i = 0; i < h_; ++i) { cells_[i].removeAt(cells_[i].length - 1); } }

    //For saving/loading.
    void loadFromData(int w, int h, bool ghostMode, List<int> cells, List<bool> ghosts, List<bool> fixeds, List<int> undos) {
        w_ = w;
        h_ = h;

        ghostMode_ = ghostMode;

        cells_ = [];
        for(int j = 0; j < h_; ++j) {
            cells_.add([]);
            for(int i = 0; i < w_; ++i) {
                int index = i + (w_ * j);
                cells_[j].add(CellData(GridValue.values[cells[index]], ghosts[index], fixeds[index]));
            }
        }

        undos_ = [];
        for(int i = 0; i < undos.length; i += 4)
            undos_.add(UndoData(undos[i], undos[i + 1], GridValue.values[undos[i + 2]], undos[i + 3] == 1));

        startFill();
    }

    List<int> listValues() {
        List<int> oup = [];
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                oup.add(cells_[j][i].val.index);

        return oup;
    }

    List<bool> listGhosts() {
        List<bool> oup = [];
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                oup.add(cells_[j][i].ghost);

        return oup;
    }

    List<bool> listFixeds() {
        List<bool> oup = [];
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                oup.add(cells_[j][i].fixed);

        return oup;
    }

    List<int> listUndos() {
        List<int> oup = [];
        for(int i = 0; i < undos_.length; ++i) {
            oup.add(undos_[i].x);
            oup.add(undos_[i].y);
            oup.add(undos_[i].oldVal.index);
            oup.add(undos_[i].wasGhost ? 1 : 0);
        }

        return oup;
    }
}
