import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/NumberSelect.dart";

class CellData {
    int val; bool ghost; bool fixed;
    GlobalKey<NumberSelectWidgetState> keyEdit; GlobalKey<NumberSelectWidgetState> keyFill;

    CellData (this.val, this.ghost, this.fixed, { this.keyEdit = null, this.keyFill = null })
        { keyEdit ??= GlobalKey<NumberSelectWidgetState>(); keyFill ??= GlobalKey<NumberSelectWidgetState>(); }
}

class UndoData { int x; int y; int oldVal; bool wasGhost; UndoData (this.x, this.y, this.oldVal, this.wasGhost) {} }

const double SUDOKU_THIN_LINE_HALF_WIDTH = 0.5;
const double SUDOKU_THICK_LINE_HALF_WIDTH = 2.0;
const double SUDOKU_TOTAL_LINE_WIDTH = (12.0 * SUDOKU_THIN_LINE_HALF_WIDTH) - (6.0 * SUDOKU_THICK_LINE_HALF_WIDTH);

class GridData
{
    double cellWidth_ = 0.0;

    List< List<CellData> > cells_ = [];
    List<UndoData> undos_ = [];

    bool editMode_ = true;
    bool ghostMode_ = false;

    GridData () {
        for(int j = 0; j < 9; ++j) {
            cells_.add([]);
            for(int i = 0; i < 9; ++i)
                cells_.last.add(CellData(-1, false, false));
        }
    }

    Widget build(double w, double h, void Function() setState) {
        cellWidth_ = w / 9;
        if(h / 9 < cellWidth_)
            cellWidth_ = h / 9;

        return Column(children: List<Widget>.generate(9, (int y) { return Row(children: List<Widget>.generate(9, (int x) {
            Widget child;
            if(editMode_) {
                //Edit + Fixed = Can change (and show the value).
                if(cells_[y][x].fixed) {
                    child = NumberSelectWidget(
                        cells_[y][x].val, (int newVal) {
                            cells_[y][x].val = newVal;
                            cells_[y][x].fixed = (newVal != -1);

                            AutoSaveLoad.save(Puzzle.Sudoku);
                        },
                        key: cells_[y][x].keyEdit,
                        width: () { return cellWidth_; }, height: () { return cellWidth_; },
                        styleFunc: () { return genTextStyle(x, y); },
                        withBlank: true, withZero: false, withType: false
                    );
                }
                //Edit + !Fixed = Can change (but don't show the value).
                else {
                    child = NumberSelectWidget(
                        -1, (int newVal) {
                            cells_[y][x].val = newVal;
                            cells_[y][x].fixed = (newVal != -1);

                            AutoSaveLoad.save(Puzzle.Sudoku);
                        },
                        key: cells_[y][x].keyEdit,
                        width: () { return cellWidth_; }, height: () { return cellWidth_; },
                        styleFunc: () { return genTextStyle(x, y); },
                        withBlank: true, withZero: false, withType: false
                    );
                }
            }
            else {
                //!Edit + Fixed = Cannot change.
                if(cells_[y][x].fixed) {
                    child = NumberSelectWidget(
                        cells_[y][x].val, (int) {},
                        key: cells_[y][x].keyFill, disabled: true,
                        width: () { return cellWidth_; }, height: () { return cellWidth_; },
                        styleFunc: () { return genTextStyle(x, y, true); }
                    );
                }
                //!Edit + !Fixed = Can change.
                else {
                    child = NumberSelectWidget(
                        cells_[y][x].val, (int newVal) {
                            undos_.add(UndoData(x, y, cells_[y][x].val, cells_[y][x].ghost));

                            cells_[y][x].val = newVal;
                            cells_[y][x].ghost = ghostMode_;

                            AutoSaveLoad.save(Puzzle.Sudoku);
                            rebuildAll(skipKey: cells_[y][x].keyFill);
                        },
                        key: cells_[y][x].keyFill,
                        width: () { return cellWidth_; }, height: () { return cellWidth_; },
                        styleFunc: () { return genTextStyle(x, y, true); },
                        withBlank: true, withZero: false, withType: false
                    );
                }
            }

            return Stack(children: [
                Container(
                    width: cellWidth_, height: cellWidth_,
                    decoration: BoxDecoration(border: Border(
                        top: BorderSide(width: ((y % 3 == 0) ? SUDOKU_THICK_LINE_HALF_WIDTH : SUDOKU_THIN_LINE_HALF_WIDTH)),
                        bottom: BorderSide(width: ((y % 3 == 2) ? SUDOKU_THICK_LINE_HALF_WIDTH : SUDOKU_THIN_LINE_HALF_WIDTH)),
                        left: BorderSide(width: ((x % 3 == 0) ? SUDOKU_THICK_LINE_HALF_WIDTH : SUDOKU_THIN_LINE_HALF_WIDTH)),
                        right: BorderSide(width: ((x % 3 == 2) ? SUDOKU_THICK_LINE_HALF_WIDTH : SUDOKU_THIN_LINE_HALF_WIDTH))
                    ))
                ),
                child
            ]);
        })); }));
    }

    TextStyle genTextStyle(int x, int y, [ bool checkWrong = false ]) {
        bool isWrong = false;
        if(cells_[y][x].val != -1 && checkWrong) {
            for(int y2 = 0; !isWrong && y2 < 9; ++y2)
                if(y2 != y && cells_[y][x].val == cells_[y2][x].val)
                    isWrong = true;
            for(int x2 = 0; !isWrong && x2 < 9; ++x2)
                if(x2 != x && cells_[y][x].val == cells_[y][x2].val)
                    isWrong = true;

            List<int> tl = [ (x / 3).floor() * 3, (y / 3).floor() * 3 ];
            for(int dx = 0; !isWrong && dx < 3; ++dx)
                for(int dy = 0; !isWrong && dy < 3; ++dy)
                    if((tl[0] + dx != x || tl[1] + dy != y) && cells_[y][x].val == cells_[tl[1] + dy][tl[0] + dx].val)
                        isWrong = true;
        }

        if(cells_[y][x].ghost)
            return TextStyle(fontSize: cellWidth_ / 2.0, color: (isWrong ? Color(0xffff8080) : Color(0xff808080)));
        else
            return TextStyle(fontSize: cellWidth_ / 2.0, fontWeight: FontWeight.bold, color: (isWrong ? Color(0xffff0000) : Color(0xff000000)));
    }

    void toggleMode() {
        if(editMode_)
            startFill();
        else {
            clearGhosts();
            startEdit();
        }
    }
    void startFill() { editMode_ = false; }
    void startEdit() { editMode_ = true; }
    bool editMode() { return editMode_; }

    void rebuildAll({ GlobalKey<NumberSelectWidgetState> skipKey = null }) {
        for(int j = 0; j < 9; ++j) {
            for(int i = 0; i < 9; ++i) {
                if(cells_[j][i].keyFill.currentState != null && cells_[j][i].keyFill != skipKey) {
                    //Force-update the printed value in the NS.
                    cells_[j][i].keyFill.currentState.value_ = cells_[j][i].val;
                    cells_[j][i].keyFill.currentState.setState(() {});
                }
                if(cells_[j][i].keyEdit.currentState != null && cells_[j][i].keyEdit != skipKey) {
                    //Force-update the printed value in the NS.
                    cells_[j][i].keyEdit.currentState.value_ = cells_[j][i].val;
                    cells_[j][i].keyEdit.currentState.setState(() {});
                }
            }
        }
    }

    void clear(bool incFixed) {
        for(int j = 0; j < 9; ++j)
            for(int i = 0; i < 9; ++i)
                if(incFixed || !cells_[j][i].fixed)
                    cells_[j][i] = CellData(-1, false, false, keyEdit: cells_[j][i].keyEdit, keyFill: cells_[j][i].keyFill);

        rebuildAll();
        undos_ = [];
    }

    void clearGhosts({ bool removeFirst = true }) {
        //Keep calling the undo() function (because it sorts out the forced updating) until there are the right number left.
        while(undos_.isNotEmpty && isGhost(undos_.last.x, undos_.last.y) && (removeFirst || (undos_.length > 1 && isGhost(undos_[undos_.length - 2].x, undos_[undos_.length - 2].y))))
            undo();

        ghostMode_ = !removeFirst;
    }

    void keepGhosts() {
        for(int i = 0; i < undos_.length; ++i)
            undos_[i].wasGhost = false;

        for(int j = 0; j < 9; ++j)
            for(int i = 0; i < 9; ++i)
                cells_[j][i].ghost = false;

        ghostMode_ = false;
    }

    void undo() {
        if(undos_.isNotEmpty) {
            cells_[undos_.last.y][undos_.last.x].val = undos_.last.oldVal;
            cells_[undos_.last.y][undos_.last.x].ghost = undos_.last.wasGhost;

            rebuildAll();
            undos_.removeAt(undos_.length - 1);
        }
    }

    int w() { return 9; }
    int h() { return 9; }
    double cellWidth() { return cellWidth_; }
    double cellHeight() { return cellWidth_; }

    bool isGhost(int x, int y) { if(x >= 0 && x < 9 && y >= 0 && y < 9) { return cells_[y][x].ghost; } else { return false; } }

    //For saving/loading.
    void loadFromData(bool ghostMode, List<int> cells, List<bool> ghosts, List<bool> fixeds, List<int> undos) {
        ghostMode_ = ghostMode;

        cells_ = [];
        for(int j = 0; j < 9; ++j) {
            cells_.add([]);
            for(int i = 0; i < 9; ++i) {
                int index = i + (9 * j);
                cells_.last.add(CellData(cells[index], ghosts[index], fixeds[index]));
            }
        }

        undos_ = [];
        for(int i = 0; i < undos.length; i += 4)
            undos_.add(UndoData(undos[i], undos[i + 1], undos[i + 2], undos[i + 3] == 1));

        startFill();
    }

    List<int> listValues() {
        List<int> oup = [];
        for(int j = 0; j < 9; ++j)
            for(int i = 0; i < 9; ++i)
                oup.add(cells_[j][i].val);

        return oup;
    }

    List<bool> listGhosts() {
        List<bool> oup = [];
        for(int j = 0; j < 9; ++j)
            for(int i = 0; i < 9; ++i)
                oup.add(cells_[j][i].ghost);

        return oup;
    }

    List<bool> listFixeds() {
        List<bool> oup = [];
        for(int j = 0; j < 9; ++j)
            for(int i = 0; i < 9; ++i)
                oup.add(cells_[j][i].fixed);

        return oup;
    }

    List<int> listUndos() {
        List<int> oup = [];
        for(int i = 0; i < undos_.length; ++i) {
            oup.add(undos_[i].x);
            oup.add(undos_[i].y);
            oup.add(undos_[i].oldVal);
            oup.add(undos_[i].wasGhost ? 1 : 0);
        }

        return oup;
    }
}
