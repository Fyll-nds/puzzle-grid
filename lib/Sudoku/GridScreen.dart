import "package:flutter/material.dart";

import "../Common/ToggleButton.dart";

import "GridData.dart";

import "EditGrid.dart";
import "FillGrid.dart";

class GridScreenWidget extends StatefulWidget
{
    final GridData grid;

    GridScreenWidget (this.grid, { Key key }) : super(key: key) {}
    GridScreenWidgetState createState() { return GridScreenWidgetState(grid); }
}

class GridScreenWidgetState extends State<GridScreenWidget>
{
    GridData grid_;

    GridScreenWidgetState (this.grid_) {}

    Widget build(BuildContext ctx) {
        if(grid_.editMode())
            return EditGridWidget(grid_, toggleFunc: toggleMode);
        else
            return FillGridWidget(grid_, toggleFunc: toggleMode);
    }

    void toggleMode() { grid_.toggleMode(); setState(() {}); }
}
