import "package:flutter/material.dart";

import "../Common/ToggleButton.dart";

import "GridData.dart";

double SIZE_BTN_WIDTH = 32.0;

class EditGridWidget extends StatefulWidget
{
    final GridData grid;

    final void Function() toggleFunc;

    EditGridWidget (this.grid, { Key key, this.toggleFunc = null }) : super(key: key) {}
    EditGridWidgetState createState() { return EditGridWidgetState(grid, toggleFunc); }
}

class EditGridWidgetState extends State<EditGridWidget>
{
    GridData grid_;

    void Function() toggleFunc;

    EditGridWidgetState (this.grid_, this.toggleFunc) {}

    Widget build(BuildContext ctx) {
        return ListView(children: [
            grid_.build(gridWidth(ctx), gridHeight(ctx), () { setState(() {}); }),
            Divider(),
            Row(children: [
                ToggleButtonWidget(Text("Reset Grid (hold)"), 0.5, h: 48.0, onLongPress: () { grid_.clear(true); setState(() {}); }),
                ToggleButtonWidget(Text("Fill Grid"), 0.5, h: 48.0, onPress: toggleFunc)
            ])
        ]);
    }

    double gridWidth(BuildContext ctx) { return MediaQuery.of(ctx).size.width; }
    double gridHeight(BuildContext ctx) { return gridWidth(ctx); }
}
