import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/GhostButton.dart";
import "../Common/ToggleButton.dart";

import "GridData.dart";

class FillGridWidget extends StatefulWidget
{
    final GridData grid;

    final void Function() toggleFunc;

    FillGridWidget (this.grid, { Key key, this.toggleFunc = null }) : super(key: key) {}
    FillGridWidgetState createState() { return FillGridWidgetState(grid, toggleFunc); }
}

class FillGridWidgetState extends State<FillGridWidget>
{
    GridData grid_;

    void Function() toggleFunc;

    GlobalKey ghostKey_ = GlobalKey();

    FillGridWidgetState (this.grid_, this.toggleFunc) {}

    @override Widget build(BuildContext ctx) {
        return ListView(children: [
            GhostButtonWidget(
                1.0,
                key: ghostKey_,
                startPressed: grid_.ghostMode_,
                onPress: () { grid_.ghostMode_ = true; setState(() {}); },
                onClear: (bool allButFirst) { grid_.clearGhosts(removeFirst: !allButFirst); setState(() {}); },
                onKeep: () { grid_.keepGhosts(); setState(() {}); },
                iconFill: (int index) { return Container(
                    color: Color(0xffd0d0d0),
                    child: Align(child: Text(
                        (index + 1).toString(),
                        style: TextStyle(fontSize: ((grid_.cellHeight() > 48.0) ? 24.0 : (grid_.cellHeight() / 2.0)), fontWeight: FontWeight.bold)
                    ))
                ); }
            ),
            Divider(),
            grid_.build(gridWidth(ctx), gridHeight(ctx), () { setState(() {}); }),
            Divider(),
            ToggleButtonWidget(Container(width: 32.0, height: 32.0, child: Icon(Icons.undo)), 1.0, onPress: undo),
            Row(children: [
                ToggleButtonWidget(Text("Clear Grid (hold)"), 0.5, h: 48.0, onLongPress: clearGrid),
                ToggleButtonWidget(Text("Edit Grid"), 0.5, h: 48.0, onPress: toggleFunc)
            ])
        ]);
    }

    void undo() {
        grid_.undo();
        AutoSaveLoad.save(Puzzle.Sudoku);

        setState(() {});
    }

    void clearGrid() {
        grid_.clear(false);
        AutoSaveLoad.save(Puzzle.Sudoku);

        setState(() {});
    }

    double gridWidth(BuildContext ctx) { return MediaQuery.of(ctx).size.width; }
    double gridHeight(BuildContext ctx) { return gridWidth(ctx); }
}
