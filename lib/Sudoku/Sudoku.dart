import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/PuzzleWidgetState.dart";
import "../Common/SaveLoad.dart";
import "../Common/AutoSaveLoad.dart";
import "../Common/SaveLoadScreen.dart";

import "SudokuData.dart";

import "GridScreen.dart";

class SudokuWidget extends StatefulWidget
{
    final SudokuData data;

    SudokuWidget (this.data, { Key key }) : super(key: key) {}
    SudokuWidgetState createState() { return SudokuWidgetState(data); }
}

class SudokuWidgetState extends PuzzleWidgetState<SudokuWidget>
{
    SudokuData data_;

    SudokuWidgetState (this.data_) {
        initTabs([ "Grid" ]);

        AutoSaveLoad.setSaveFunc(Puzzle.Sudoku, save);
        AutoSaveLoad.load(Puzzle.Sudoku, loadFromData, () { setState(() {}); });
    }

    void onTabChange() { data_.grid.clearGhosts(); }

    void loadFromData(Map<String, dynamic> data) {
        data_.grid.loadFromData(data["ghostMode"], List<int>.from(data["vals"]), List<bool>.from(data["ghosts"]), List<bool>.from(data["fixeds"]), List<int>.from(data["undos"]));
    }

    void save(String fileName, String key) {
        SaveLoad.save(fileName, key, Map<String, dynamic>.fromEntries([
            MapEntry<String, dynamic>("ghostMode", data_.grid.ghostMode_),
            MapEntry<String, dynamic>("vals", data_.grid.listValues()),
            MapEntry<String, dynamic>("ghosts", data_.grid.listGhosts()),
            MapEntry<String, dynamic>("fixeds", data_.grid.listFixeds()),
            MapEntry<String, dynamic>("undos", data_.grid.listUndos())
        ]));
    }

    List<Widget> tabWidgets() {
        return [
            GridScreenWidget(data_.grid),
            SaveLoadScreenWidget(saveFunc: save, loadFunc: loadFromData, fileName: SUDOKU_SAVE_FILE_NAME)
        ];
    }
}
