import "package:flutter/material.dart";

class PuzzleButtonWidget extends StatefulWidget
{
    final String imgName;
    final String lbl;
    final void Function() onPress;

    PuzzleButtonWidget (this.imgName, this.lbl, this.onPress, { GlobalKey key }) : super(key: key) {}
    PuzzleButtonWidgetState createState() { return PuzzleButtonWidgetState(imgName, lbl, onPress); }
}

class PuzzleButtonWidgetState extends State<PuzzleButtonWidget>
{
    String imgName;
    String lbl;
    void Function() onPressFunc;

    PuzzleButtonWidgetState (this.imgName, this.lbl, this.onPressFunc) {}

    @override Widget build(BuildContext ctx) {
        Size dim = Size(MediaQuery.of(ctx).size.width - 8.0, MediaQuery.of(ctx).size.width / 4.0);

        return Container(
            width: dim.width, height: dim.height,
            padding: EdgeInsets.all(4.0),
            child: GestureDetector(
                child: Stack(children: [
                    Container(decoration: BoxDecoration(
                        image: DecorationImage(image: AssetImage(imgName), fit: BoxFit.cover),
                        borderRadius: BorderRadius.circular(dim.height / 8.0)
                    )),
                    Container(decoration: BoxDecoration(
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft, end: Alignment.centerRight,
                            colors: [Color(0x00ffffff), Color(0xffffffff)],
                            stops: [0.0, 0.6]
                        ),
                        borderRadius: BorderRadius.circular(dim.height / 8.0)
                    )),
                    Container(
                        decoration: BoxDecoration(
                            gradient: LinearGradient(
                                begin: Alignment.topCenter, end: Alignment.bottomCenter,
                                colors: [Color(0xffffffff), Color(0x00ffffff), Color(0xffffffff)]
                            ),
                            borderRadius: BorderRadius.circular(dim.height / 8.0),
                            border: Border.all(width: 1.0)
                        ),
                        child: Align(child: Text(
                            lbl + " ",
                            style: TextStyle(fontSize: dim.height / 4.0, color: Color(0xff404040), fontWeight: FontWeight.bold)
                        ), alignment: Alignment.centerRight)
                    )
                ]),
                onTapUp: (_) { onPressFunc(); }
            )
        );
    }
}
