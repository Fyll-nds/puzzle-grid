import "package:flutter/material.dart";

class TextClueData
{
    String question; int num; bool hoz; bool selected;
    TextClueData ({ this.question = "", this.num = 0, this.hoz = true, this.selected = false }) {}
}

class TextClueWidget extends StatefulWidget
{
    final TextClueData data;

    final Function() onSelect;
    final Function() onDeselect;

    TextClueWidget ({ Key key, this.data = null, this.onSelect = null, this.onDeselect = null }) : super(key: key) {}
    TextClueWidgetState createState() { return TextClueWidgetState(this.data, this.onSelect, this.onDeselect); }
}

class TextClueWidgetState extends State<TextClueWidget>
{
    TextClueData data_;

    void Function() onSelectFunc = null;
    void Function() onDeselectFunc = null;

    bool get isSelectable { return (onSelectFunc != null); }

    TextClueWidgetState (this.data_, this.onSelectFunc, this.onDeselectFunc) {
        data_ ??= TextClueData();
    }

    @override Widget build(BuildContext ctx) {
        TextStyle style = DefaultTextStyle.of(ctx).style;

        if(isSelectable) {
            return GestureDetector(
                child: Container(
                    width: MediaQuery.of(ctx).size.width,
                    padding: EdgeInsets.symmetric(vertical: style.fontSize / 2.0, horizontal: style.fontSize),
                    decoration: BoxDecoration(border: (data_.selected ? Border.all(color: Color(0xff000000), width: 2.0) : Border())),
                    child: RichText(text: TextSpan(style: style, children: [
                        TextSpan(text: data_.num.toString() + (data_.hoz ? "A" : "D") + ": ", style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: data_.question.toString())
                    ]))
                ),
                onTap: onTap
            );
        }
        else {
            return GestureDetector(
                child: Container(
                    width: MediaQuery.of(ctx).size.width,
                    padding: EdgeInsets.symmetric(vertical: style.fontSize, horizontal: style.fontSize * 2.0),
                    color: Color(0xffe0e0e0),
                    child: RichText(text: TextSpan(style: style, children: [
                        TextSpan(text: data_.num.toString() + (data_.hoz ? "A" : "D") + ": ", style: TextStyle(fontWeight: FontWeight.bold)),
                        TextSpan(text: data_.question.toString())
                    ]))
                ),
                onTap: onTap
            );
        }
    }

    void onTap() {
        if(isSelectable) {
            if(data_.selected) {
                data_.selected = false;
                onDeselectFunc();
            }
            else {
                data_.selected = true;
                onSelectFunc();
            }

            setState(() {});
        }
    }
}
