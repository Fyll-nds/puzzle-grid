import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/PuzzleWidgetState.dart";
import "../Common/Settings.dart";
import "../Common/SaveLoad.dart";
import "../Common/AutoSaveLoad.dart";
import "../Common/SaveLoadScreen.dart";

import "CrosswordData.dart";
import "GridSymmetry.dart" show SymmetryType;

import "ClueSnippet.dart";
import "GridScreen.dart";
import "ClueScreen.dart";

class CrosswordWidget extends StatefulWidget
{
    final CrosswordData data;

    CrosswordWidget (this.data, { Key key }) : super(key: key) {}
    CrosswordWidgetState createState() { return CrosswordWidgetState(data); }
}

class CrosswordWidgetState extends PuzzleWidgetState<CrosswordWidget>
{
    CrosswordData data_;

    CrosswordWidgetState (this.data_) {
        initTabs([ "Grid", "Clues" ]);

        Settings.addBool(ThingToSave.Snip, false, (bool newVal) { setState(() { data_.clue.canSnip = newVal; }); });
        Settings.addInt(ThingToSave.Symmetry, 0, (int newVal) { setState(() { data_.gridSym.type_ = SymmetryType.values[newVal]; }); });

        AutoSaveLoad.setSaveFunc(Puzzle.Crossword, save);
        AutoSaveLoad.load(Puzzle.Crossword, loadFromData, () { setState(() {}); });
    }

    void loadFromData(Map<String, dynamic> data) {
        data_.grid.loadFromData(data["width"], data["height"], List<bool>.from(data["white"]), List<String>.from(data["chars"]));
        data_.clue.loadFromData(data["clue_path"], data["text_clues"], () {});
    }

    void save(String fileName, String key) {
        SaveLoad.save(fileName, key, Map<String, dynamic>.fromEntries([
            MapEntry<String, dynamic>("width", data_.grid.w()), MapEntry<String, dynamic>("height", data_.grid.h()),
            MapEntry<String, dynamic>("white", data_.grid.listWhite()), MapEntry<String, dynamic>("chars", data_.grid.listChars()),
            MapEntry<String, dynamic>("clue_path", data_.clue.filePath()), MapEntry<String, dynamic>("text_clues", data_.clue.isText())
        ]));
    }

    Widget topWidget() { return ClueSnippetWidget(clue: data_.clue); }
    List<Widget> tabWidgets() {
        return [
            GridScreenWidget(grid: data_.grid, clue: data_.clue, gridSym: data_.gridSym),
            ClueScreenWidget(clue: data_.clue),
            SaveLoadScreenWidget(saveFunc: save, loadFunc: loadFromData, fileName: CROSSWORD_SAVE_FILE_NAME)
        ];
    }
}
