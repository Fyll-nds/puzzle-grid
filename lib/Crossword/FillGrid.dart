import "package:flutter/material.dart";
import "package:flutter/services.dart";
import "package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart";

import "../Puzzles.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/ToggleButton.dart";

import "GridData.dart";
import "ClueData.dart";

class FillGridWidget extends StatefulWidget
{
    final GridData grid;
    final ClueData clue;
    final ScrollController scrollControl;

    final void Function() toggleFunc;

    FillGridWidget ({ Key key, this.grid = null, this.clue = null, this.scrollControl = null, this.toggleFunc = null }) : super(key: key) {}
    FillGridWidgetState createState() { return FillGridWidgetState(grid, clue, scrollControl, toggleFunc); }
}

class FillGridWidgetState extends State<FillGridWidget>
{
    GridData grid_;
    ClueData clue_;
    CellData selectedCell_ = null;

    void Function() toggleFunc;

    bool hoz_ = false;
    FocusNode focus_;

    TextEditingController txt_;
    bool ignoreTextInput_ = false;

    ScrollController scrollCtrl_;

    FillGridWidgetState (this.grid_, this.clue_, this.scrollCtrl_, this.toggleFunc) {
        focus_ = FocusNode(onKey: onHardKey);
        txt_ = TextEditingController(text: "a");
    }

    @override void initState() {
        super.initState();
        KeyboardVisibility.onChange.listen((bool visible) { if(!visible) { deselectCell(); } });
    }

    @override Widget build(BuildContext ctx) {
        ignoreTextInput_ = false;

        return ListView(
            controller: scrollCtrl_,
            children: [
                Stack(children: [
                    EditableText(
                        focusNode: focus_, controller: txt_,
                        autocorrect: false, enableSuggestions: false,
                        style: TextStyle(), cursorColor: Colors.transparent, backgroundCursorColor: Colors.transparent,
                        showCursor: false, showSelectionHandles: false,
                        onChanged: onSoftKey
                    ),
                    GestureDetector(
                        child: grid_.build(gridWidth(ctx), gridHeight(ctx)),
                        onTapDown: onTap,
                        onVerticalDragStart: null, onVerticalDragUpdate: null, onVerticalDragEnd: null
                    )
                ]),
                Divider(),
                Row(children: [
                    ToggleButtonWidget(Text("Clear Grid (hold)"), 0.5, h: 48.0, onLongPress: () { setState(() { grid_.clearChars(); }); }),
                    ToggleButtonWidget(Text("Edit Grid"), 0.5, h: 48.0, onPress: toggleFunc)
                ])
            ]
        );
    }

    void onTap(TapDownDetails details) {
        CellData newCell = grid_.atPos(details.localPosition);
        if(newCell.white) {
            if(selectedCell_ != null)
                selectedCell_.selected = false;

            selectedCell_ = newCell;
            selectedCell_.selected = true;

            //Default to horizontal, unless you are already horizontal, and unless it's the start of a down word.
            //If you're selecting another cell in the same line, switch direction.
            if(selectedCell_.inLine && (selectedCell_.nbs.last(!hoz_) != null || selectedCell_.nbs.next(!hoz_) != null))
                hoz_ = !hoz_;
            //Else if you're selecting a cell in a column and it's the start of a vertical word (and not the start of a horizontal word).
            else if(selectedCell_.nbs.last(false) == null && selectedCell_.nbs.next(false) != null && selectedCell_.nbs.last(true) != null)
                hoz_ = false;
            //Else, default to horizontal if it's an option.
            else
                hoz_ = (selectedCell_.nbs.last(true) != null || selectedCell_.nbs.next(true) != null);

            grid_.selectCellAtPos(details.localPosition, hoz_);

            //Find out the number of the line the selected cell is in (for text clues).
            CellData cell = grid_.atPos(details.localPosition);
            while(cell.n == 0)
                cell = grid_.at(cell.nbs.last(hoz_).x, cell.nbs.last(hoz_).y);
            clue_.selectTextClueFromGrid(cell.n, hoz_);

            //Scroll to make sure the selected cell is visible.
            scrollCtrl_.animateTo(details.localPosition.dy - grid_.cellHeight(), duration: Duration(milliseconds: 1024), curve: Curves.decelerate);

            focus_.requestFocus();

            setState(() {});
        }
        else
            deselectCell();
    }

    void deselectCell() {
        if(selectedCell_ != null) {
            selectedCell_ = null;
            grid_.deselectCell();
            clue_.selectTextClue(-1);
            focus_.unfocus();

            setState(() {});
        }
    }

    bool onHardKey(FocusNode focus, RawKeyEvent key) {
        if(key.logicalKey == LogicalKeyboardKey.backspace) {
            backspace();

            AutoSaveLoad.save(Puzzle.Crossword);
            setState(() {});

            return true;
        }
        else if(key.character != null) {
            char(key.character);

            AutoSaveLoad.save(Puzzle.Crossword);
            setState(() {});

            return true;
        }
    }

    void onSoftKey(String newStr) {
        if(!ignoreTextInput_) {
            ignoreTextInput_ = true;
            if(newStr.length == 0)
                backspace();
            else if(newStr.length == 2)
                char(newStr[newStr.length - 1]);

            txt_.text = "a";
            txt_.selection = TextSelection.fromPosition(TextPosition(offset: 1));

            AutoSaveLoad.save(Puzzle.Crossword);
            setState(() {});
        }
    }

    void backspace() {
        selectedCell_.c = "";

        if(selectedCell_.nbs.last(hoz_) != null) {
            selectedCell_.selected = false;
            selectedCell_ = grid_.atCoord(selectedCell_.nbs.last(hoz_));
            selectedCell_.selected = true;

            if(!hoz_)
                scrollCtrl_.animateTo(scrollCtrl_.offset - grid_.cellHeight(), duration: Duration(milliseconds: 1024), curve: Curves.decelerate);
        }
    }

    void char(String c) {
        selectedCell_.c = c.toUpperCase();

        if(selectedCell_.nbs.next(hoz_) != null) {
            selectedCell_.selected = false;
            selectedCell_ = grid_.atCoord(selectedCell_.nbs.next(hoz_));
            selectedCell_.selected = true;

            if(!hoz_)
                scrollCtrl_.animateTo(scrollCtrl_.offset + grid_.cellHeight(), duration: Duration(milliseconds: 1024), curve: Curves.decelerate);
        }
    }

    double gridWidth(BuildContext ctx) { return MediaQuery.of(ctx).size.width; }
    double gridHeight(BuildContext ctx) { return gridWidth(ctx) * grid_.h() / grid_.w(); }
}
