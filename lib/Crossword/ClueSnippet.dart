import "package:flutter/material.dart";

import "ClueData.dart";
import "TextClue.dart";

class ClueSnippetWidget extends StatefulWidget
{
    final ClueData clue;

    ClueSnippetWidget ({ Key key, this.clue }) : super(key: key) {}
    ClueSnippetWidgetState createState() { return ClueSnippetWidgetState(clue); }
}

class ClueSnippetWidgetState extends State<ClueSnippetWidget>
{
    ClueData clue_;

    ClueSnippetWidgetState (this.clue_) {}

    Widget build(BuildContext ctx) {
        //Tell the clue data how to redraw this widget.
        if(clue_.redrawFunc == null)
            clue_.redrawFunc = () { setState(() {}); };

        //Don't do anything if there's no image or snip.
        if(!clue_.isLoaded() || !clue_.isSnip())
            return Container();
        else if(clue_.isText())
            return TextClueWidget(key: UniqueKey(), data: clue_.selectedTextClue());
        else {
            return Column(children: [
                Container(width: 1, height: 4),
                Row(children: [
                    Expanded(child: Container()),
                    Container(
                        width: clue_.snipDim.width, height: clue_.snipDim.height,
                        child: Image.file(
                            clue_.file,
                            scale: clue_.imgDim.width / MediaQuery.of(ctx).size.width,
                            fit: BoxFit.none,
                            alignment: clue_.snipToPos(ctx)
                        )
                    ),
                    Expanded(child: Container())
                ]),
                Container(width: 1, height: 4)
            ]);
        }
    }
}
