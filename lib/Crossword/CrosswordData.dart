import "GridData.dart";
import "GridSymmetry.dart";
import "ClueData.dart";

class CrosswordData
{
    GridData grid = GridData(15, 15);
    GridSymmetry gridSym = GridSymmetry();
    ClueData clue = ClueData();
}