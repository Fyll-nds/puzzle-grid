import "package:flutter/material.dart";

import "../Common/ToggleButton.dart";

import "GridData.dart";
import "ClueData.dart";
import "GridSymmetry.dart";

import "EditGrid.dart";
import "FillGrid.dart";

class GridScreenWidget extends StatefulWidget
{
    final GridData grid;
    final ClueData clue;
    final GridSymmetry gridSym;

    GridScreenWidget ({ Key key, this.grid = null, this.clue = null, this.gridSym = null }) : super(key: key) {}
    GridScreenWidgetState createState() { return GridScreenWidgetState(grid, clue, gridSym); }
}

class GridScreenWidgetState extends State<GridScreenWidget>
{
    GridData grid_;
    ClueData clue_;
    GridSymmetry gridSym_;

    ScrollController scrollCtrl_;

    GridScreenWidgetState (this.grid_, this.clue_, this.gridSym_) {}

    @override void initState() {
        scrollCtrl_ = ScrollController();
        super.initState();
    }

    @override void dispose() {
        scrollCtrl_.dispose();
        super.dispose();
    }

    Widget build(BuildContext ctx) {
        if(grid_.editMode())
            return EditGridWidget(grid: grid_, gridSym: gridSym_, toggleFunc: toggleMode);
        else
            return FillGridWidget(grid: grid_, clue: clue_, scrollControl: scrollCtrl_, toggleFunc: toggleMode);
    }

    void toggleMode() { setState(() { grid_.toggleMode(); }); }
}
