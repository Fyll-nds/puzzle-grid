import "package:flutter/material.dart";

import "../Common/Settings.dart";
import "GridData.dart";

enum SymmetryType { None, Rot_90, Rot_180, Ref_Y, Ref_X, Ref_MainDiag, Ref_OffDiag, Ref_Plus, Ref_Cross, Ref_All }

double SIZE_BTN_WIDTH = 32.0;

class GridSymmetry
{
    SymmetryType type_ = SymmetryType.None;

    Widget build(BuildContext ctx, void Function(void Function()) updateParent, bool isSquare) {
        return Column(children: [
            Row(children: [
                Expanded(child: Container()),
                Text("Symmetry: "),
                DropdownButton<SymmetryType>(
                    value: type_,
                    onChanged: (SymmetryType newValue) { type_ = newValue; Settings.setInt(ThingToSave.Symmetry, newValue.index); updateParent(() {}); },
                    selectedItemBuilder: smallItemTextList,
                    items: [
                        DropdownMenuItem<SymmetryType>(value: SymmetryType.None, child: Text("None")),

                        genItem(SymmetryType.Rot_90, isSquare),
                        genItem(SymmetryType.Rot_180, true),

                        genItem(SymmetryType.Ref_Y, true),
                        genItem(SymmetryType.Ref_X, true),
                        genItem(SymmetryType.Ref_MainDiag, isSquare),
                        genItem(SymmetryType.Ref_OffDiag, isSquare),

                        genItem(SymmetryType.Ref_Plus, true),
                        genItem(SymmetryType.Ref_Cross, isSquare),
                        genItem(SymmetryType.Ref_All, isSquare)
                    ]
                ),
                Expanded(child: Container())
            ]),
            (!isSquare && needsSquare(type_)
                ? Center(child: Text("This symmetry needs a square grid."))
                : Container()
            )
        ]);
    }

    bool needsSquare(SymmetryType type) {
        return (type == SymmetryType.Rot_90 || type == SymmetryType.Ref_MainDiag || type == SymmetryType.Ref_OffDiag || type == SymmetryType.Ref_Cross || type == SymmetryType.Ref_All);
    }

    List<Coord> toToggle(Coord p, int wGrid, int hGrid, [SymmetryType type = null]) {
        if(type == null)
            type = type_;

        List<Coord> oup = [p];

        if(type == SymmetryType.Rot_180)
            oup.add(Coord(wGrid - 1 - p.x, hGrid - 1 - p.y));
        else if(type == SymmetryType.Ref_Y)
            oup.add(Coord(wGrid - 1 - p.x, p.y));
        else if(type == SymmetryType.Ref_X)
            oup.add(Coord(p.x, hGrid - 1 - p.y));
        else if(type == SymmetryType.Ref_Plus) {
            oup.add(Coord(wGrid - 1 - p.x, p.y));
            oup.add(Coord(p.x, hGrid - 1 - p.y));
            oup.add(Coord(wGrid - 1 - p.x, hGrid - 1 - p.y));
        }
        //The rest of these require the grid to be symmetric.
        else if(wGrid == hGrid) {
            if(type == SymmetryType.Rot_90) {
                oup.add(Coord(wGrid - 1 - p.y, p.x));
                oup.add(Coord(wGrid - 1 - p.x, hGrid - 1 - p.y));
                oup.add(Coord(p.y, hGrid - 1 - p.x));
            }
            else if(type == SymmetryType.Ref_MainDiag)
                oup.add(Coord(p.y, p.x));
            else if(type == SymmetryType.Ref_OffDiag)
                oup.add(Coord(wGrid - 1 - p.y, hGrid - 1 - p.x));
            else if(type == SymmetryType.Ref_Cross) {
                oup.add(Coord(wGrid - 1 - p.y, hGrid - 1 - p.x));
                oup.add(Coord(p.y, p.x));
                oup.add(Coord(wGrid - 1 - p.x, hGrid - 1 - p.y));
            }
            else if(type == SymmetryType.Ref_All) {
                oup.add(Coord(wGrid - 1 - p.x, p.y));
                oup.add(Coord(p.y, p.x));
                oup.add(Coord(wGrid - 1 - p.y, p.x));
                oup.add(Coord(p.y, hGrid - 1 - p.x));
                oup.add(Coord(wGrid - 1 - p.y, hGrid - 1 - p.x));
                oup.add(Coord(p.x, hGrid - 1 - p.y));
                oup.add(Coord(wGrid - 1 - p.x, hGrid - 1 - p.y));
            }
        }

        return oup;
    }

    //Returns [ width, height, (0 = no-diag; 1 = main-diag; 2 = off-diag; 3 = both-diag) ]
    List<int> minArea(int wGrid, int hGrid, [ SymmetryType type = null ]) {
        if(type == null)
            type = type_;

        if(type == SymmetryType.None)
            return [ wGrid, hGrid, 0 ];
        else if(type == SymmetryType.Rot_90)
            return [ (wGrid / 2).ceil(), (hGrid / 2).ceil(), 0 ];
        else if(type == SymmetryType.Rot_180)
            return [ wGrid, (hGrid / 2).ceil(), 0 ];
        else if(type == SymmetryType.Ref_Y)
            return [ (wGrid / 2).ceil(), hGrid, 0 ];
        else if(type == SymmetryType.Ref_X)
            return [ wGrid, (hGrid / 2).ceil(), 0 ];
        else if(type == SymmetryType.Ref_MainDiag)
            return [ wGrid, hGrid, 1 ];
        else if(type == SymmetryType.Ref_OffDiag)
            return [ wGrid, hGrid, 2 ];
        else if(type == SymmetryType.Ref_Plus)
            return [ (wGrid / 2).ceil(), (hGrid / 2).ceil(), 0 ];
        else if(type == SymmetryType.Ref_Cross)
            return [ wGrid, hGrid, 3 ];
        else if(type == SymmetryType.Ref_All)
            return [ (wGrid / 2).ceil(), (hGrid / 2).ceil(), 1 ];
    }

    List<Widget> smallItemTextList(BuildContext ctx) {
        return List<Widget>.generate(SymmetryType.values.length, (int index) { return Center(child: Text(smallItemText(SymmetryType.values[index]))); });
    }

    String smallItemText(SymmetryType type) {
        if(type == SymmetryType.None)
            return "None";
        else if(type == SymmetryType.Rot_90)
            return "90\u{00B0} rotation";
        else if(type == SymmetryType.Rot_180)
            return "180\u{00B0} rotation";
        else if(type == SymmetryType.Ref_Y)
            return "Reflect about vertical";
        else if(type == SymmetryType.Ref_X)
            return "Reflect about horizontal";
        else if(type == SymmetryType.Ref_MainDiag)
            return "Reflect about main diagonal";
        else if(type == SymmetryType.Ref_OffDiag)
            return "Reflect about off diagonal";
        else if(type == SymmetryType.Ref_Plus)
            return "Reflect about both axes";
        else if(type == SymmetryType.Ref_Cross)
            return "Reflect about both diagonals";
        else if(type == SymmetryType.Ref_All)
            return "Reflect about axes and diagonals";
    }

    Widget genItem(SymmetryType type, bool enabled) {
        if(!enabled)
            return DropdownMenuItem<SymmetryType>(value: type, child: Container());

        List<Widget> row = [];

        //Draw up a little demonstration.
        Coord demoDims = Coord(6, 6);
        double demoCellHeight = 16.0;
        List<Coord> demoBase = [Coord(0, 0), Coord(0, 1)];

        List<Coord> demo = toToggle(demoBase[0], demoDims.x, demoDims.y, type);
        for(int i = 1; i < demoBase.length; ++i)
            demo.addAll(toToggle(demoBase[i], demoDims.x, demoDims.y, type));

        List< List<Color> > demoCols = List.generate(demoDims.y, (int) { return List.generate(demoDims.x, (int) { return Colors.white; }); });
        for(int i = 0; i < demo.length; ++i)
            demoCols[demo[i].y][demo[i].x] = Colors.grey;
        for(int i = 0; i < demoBase.length; ++i)
            demoCols[demoBase[i].y][demoBase[i].x] = Colors.black;

        row.add(Column(children: List<Widget>.generate(demoDims.y, (int y) { return Row(children: List<Widget>.generate(demoDims.x, (int x) {
            return Container(
                width: demoCellHeight, height: demoCellHeight,
                decoration: BoxDecoration(color: demoCols[y][x], border: Border.all(width: 1, color: Colors.black))
            );
        })); })));

        row.add(Container(width: 8, height: 1));
        row.add(Flexible(child: Text(smallItemText(type))));
        return DropdownMenuItem<SymmetryType>(value: type, child: Container(
            height: demoCellHeight * (demoDims.y + 1),
            child: Column(children: [
                Expanded(child: Container()),
                Row(children: row),
                Expanded(child: Container())
            ])
        ));
    }
}
