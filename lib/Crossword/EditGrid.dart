import "package:flutter/material.dart";

import "../Puzzles.dart";

import "../Common/AutoSaveLoad.dart";
import "../Common/ToggleButton.dart";

import "GridData.dart";
import "GridSymmetry.dart";

double SIZE_BTN_WIDTH = 32.0;

class EditGridWidget extends StatefulWidget
{
    final GridData grid;
    final GridSymmetry gridSym;

    final void Function() toggleFunc;

    EditGridWidget ({ Key key, this.grid = null, this.gridSym = null, this.toggleFunc = null }) : super(key: key) {}
    EditGridWidgetState createState() { return EditGridWidgetState(grid, gridSym, toggleFunc); }
}

class EditGridWidgetState extends State<EditGridWidget>
{
    GridData grid_;
    GridSymmetry gridSym_;

    void Function() toggleFunc;

    bool draggingWhite_ = false;

    EditGridWidgetState (this.grid_, this.gridSym_, this.toggleFunc) {}

    Widget build(BuildContext ctx) {
        return ListView(children: [
            Center(child: Text("Dimensions: " + grid_.w().toString() + " x " + grid_.h().toString())),
            vPlus(ctx, true),
            Row(children: [
                hPlus(ctx, true),
                Expanded(child: Container()),
                GestureDetector(
                    child: grid_.build(gridWidth(ctx), gridHeight(ctx)),
                    onTapDown: onTap, onPanUpdate: onDrag, onVerticalDragUpdate: onDrag
                ),
                Expanded(child: Container()),
                hPlus(ctx, false)
            ]),
            vPlus(ctx, false),
            Divider(),
            gridSym_.build(ctx, setState, (grid_.w() == grid_.h())),
            Divider(),
            Row(children: [
                ToggleButtonWidget(Text("All Black"), 1.0 / 3.0, h: 48.0, onPress: () { grid_.fillGridWithPattern((int i, int j) { return false; }); setState(() {}); }),
                ToggleButtonWidget(Text("All White"), 1.0 / 3.0, h: 48.0, onPress: () { grid_.fillGridWithPattern((int i, int j) { return true; }); setState(() {}); }),
                ToggleButtonWidget(Text("Grid"), 1.0 / 3.0, h: 48.0, onPress: () { grid_.fillGridWithPattern((int i, int j) { return (i % 2 == 0 || j % 2 == 0); }); setState(() {}); })
            ]),
            ToggleButtonWidget(Text("Fill Grid"), 1.0, h: 48.0, onPress: toggleFunc)
        ]);
    }

    Widget vPlus(BuildContext ctx, bool top) {
        return Row(children: [
            Container(width: SIZE_BTN_WIDTH),
            editButton(ctx, gridWidth(ctx) / 2.0, SIZE_BTN_WIDTH, "+", (top ? grid_.addRowTop : grid_.addRowBot)),
            editButton(ctx, gridWidth(ctx) / 2.0, SIZE_BTN_WIDTH, "-", (top ? grid_.subRowTop : grid_.subRowBot)),
            Container(width: SIZE_BTN_WIDTH)
        ]);
    }

    Widget hPlus(BuildContext ctx, bool left) {
        return Column(children: [
            editButton(ctx, SIZE_BTN_WIDTH, gridHeight(ctx) / 2.0, "+", (left ? grid_.addColLeft : grid_.addColRight)),
            editButton(ctx, SIZE_BTN_WIDTH, gridHeight(ctx) / 2.0, "-", (left ? grid_.subColLeft : grid_.subColRight))
        ]);
    }

    Widget editButton(BuildContext ctx, double w, double h, String txt, void Function() func) {
        return ToggleButtonWidget(Text(txt), w / MediaQuery.of(ctx).size.width, h: h, onPress: () { func(); setState(() {}); AutoSaveLoad.save(Puzzle.Crossword); });
    }

    void onTap(TapDownDetails details) {
        draggingWhite_ = !grid_.atPos(details.localPosition).white;
        toggleCell(details.localPosition);

        AutoSaveLoad.save(Puzzle.Crossword);
        setState(() {});
    }

    void onDrag(DragUpdateDetails details) {
        toggleCell(details.localPosition);

        AutoSaveLoad.save(Puzzle.Crossword);
        setState(() {});
    }

    void toggleCell(Offset pos) {
        List<Coord> allToToggle = gridSym_.toToggle(grid_.pToCoord(pos), grid_.w(), grid_.h());
        for(int i = 0; i < allToToggle.length; ++i)
            grid_.atCoord(allToToggle[i]).setWhite(draggingWhite_);
    }

    double gridWidth(BuildContext ctx) { return MediaQuery.of(ctx).size.width - (2.0 * SIZE_BTN_WIDTH); }
    double gridHeight(BuildContext ctx) { return gridWidth(ctx); }
}
