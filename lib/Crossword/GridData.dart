import "package:flutter/material.dart";

class Coord { int x; int y; Coord (this.x, this.y) {} }
class Neighbours
{
    Coord last_v = null; Coord last_h = null; Coord next_v = null; Coord next_h = null;
    Coord last(bool hoz) { return (hoz ? last_h : last_v); }
    Coord next(bool hoz) { return (hoz ? next_h : next_v); }
}

class CellData
{
    bool white; int n; String c;
    Neighbours nbs; bool selected = false; bool inLine = false;
    CellData ({ this.white = true, this.n = 0, this.c = "" }) {}

    void toggleWhite() { white = !white; }
    void setWhite(bool newVal) { white = newVal; }
}

class GridData
{
    int w_ = 0;
    int h_ = 0;
    double cellWidth_ = 0.0;

    List< List<CellData> > cells_ = [];

    bool editMode_ = true;

    GridData (int w, int h) : w_ = w, h_ = h {
        cells_ = [];

        for(int j = 0; j < h_; ++j) {
            cells_.add([]);
            for(int i = 0; i < w_; ++i)
                cells_[j].add(CellData());
        }
    }

    Widget build(double width, double height) {
        cellWidth_ = width / w_;
        if(height / h_ < cellWidth_)
            cellWidth_ = height / h_;

        return Column(children: List<Widget>.generate(h_, (int y) { return Row(children: List<Widget>.generate(w_, (int x) { return genCellWidget(x, y); })); }));
    }

    Widget genCellWidget(int x, int y) {
        List<Widget> layers = [
            Container(
                width: cellWidth_, height: cellWidth_,
                decoration: BoxDecoration(
                    color: (cells_[y][x].white ? ((!editMode_ && cells_[y][x].inLine) ? Colors.yellow[200] : Colors.white) : Colors.black),
                    border: ((!editMode_ && cells_[y][x].selected) ? Border.all(width: 3, color: Colors.yellow[800]) : Border.all(width: 1, color: Colors.black))
                )
            )
        ];

        if(!editMode_ && cells_[y][x].white) {
            //If no letter is present, but a number is, make the number bigger (for ease of reading).
            if(cells_[y][x].c == "" && cells_[y][x].n > 0)
                layers.add(charInCell(cells_[y][x].n.toString(), 0.6, Alignment.center, Colors.grey));
            else if(cells_[y][x].c != "") {
                layers.add(charInCell(cells_[y][x].c, 0.6, Alignment.bottomCenter, Colors.black));

                if(cells_[y][x].n > 0)
                    layers.add(charInCell(cells_[y][x].n.toString(), 0.3, Alignment.topLeft, Color(0xff707070)));
            }
        }

        return Stack(children: layers);
    }

    Widget charInCell(String c, double scale, Alignment pos, Color col) {
        return Container(
            width: cellWidth_, height: cellWidth_,
            child: Align(
                alignment: pos,
                child: Container(
                    padding: EdgeInsets.all(2),
                    child: Text(c, style: TextStyle(
                        fontSize: cellWidth_ * scale,
                        fontWeight: FontWeight.bold,
                        color: col
                    ))
                )
            )
        );
    }

    void toggleMode() { if(editMode_) { startFill(); } else { startEdit(); } }
    void startFill() { editMode_ = false; setNumbers(); }
    void startEdit() { editMode_ = true; }

    void setNumbers() {
        int index = 1;

        for(int j = 0; j < h_; ++j) {
            for(int i = 0; i < w_; ++i) {
                cells_[j][i].nbs = Neighbours();
                cells_[j][i].n = 0;

                if(cells_[j][i].white) {
                    if(i > 0 && cells_[j][i - 1].white)
                        cells_[j][i].nbs.last_h = Coord(i - 1, j);
                    if(j > 0 && cells_[j - 1][i].white)
                        cells_[j][i].nbs.last_v = Coord(i, j - 1);

                    if(i < w_ - 1 && cells_[j][i + 1].white)
                        cells_[j][i].nbs.next_h = Coord(i + 1, j);
                    if(j < h_ - 1 && cells_[j + 1][i].white)
                        cells_[j][i].nbs.next_v = Coord(i, j + 1);

                    if((cells_[j][i].nbs.next_v != null && cells_[j][i].nbs.last_v == null) || (cells_[j][i].nbs.next_h != null && cells_[j][i].nbs.last_h == null))
                        cells_[j][i].n = (index++);
                }
            }
        }
    }

    bool editMode() { return editMode_; }
    void clearChars() { for(int j = 0; j < h_; ++j) { for(int i = 0; i < w_; ++i) { cells_[j][i].c = ""; } } }

    void selectCellAt(int x, int y, bool hoz) {
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                cells_[j][i].inLine = false;

        cells_[y][x].selected = true;
        cells_[y][x].inLine = true;

        for(Coord nbs = cells_[y][x].nbs.last(hoz); nbs != null; nbs = cells_[nbs.y][nbs.x].nbs.last(hoz))
            cells_[nbs.y][nbs.x].inLine = true;
        for(Coord nbs = cells_[y][x].nbs.next(hoz); nbs != null; nbs = cells_[nbs.y][nbs.x].nbs.next(hoz))
            cells_[nbs.y][nbs.x].inLine = true;
    }
    void selectCellAtPos(Offset p, bool hoz) { selectCellAt((p.dx / cellWidth_).floor(), (p.dy / cellWidth_).floor(), hoz); }

    void deselectCell() {
        for(int j = 0; j < h_; ++j) {
            for(int i = 0; i < w_; ++i) {
                cells_[j][i].inLine = false;
                cells_[j][i].selected = false;
            }
        }
    }

    int w() { return w_; }
    int h() { return h_; }
    double cellWidth() { return cellWidth_; }
    double cellHeight() { return cellWidth_; }

    CellData at(int x, int y) { if(x >= 0 && x < w_ && y >= 0 && y < h_) { return cells_[y][x]; } else { return CellData(); } }
    CellData atCoord(Coord c) { if(c != null) { return at(c.x, c.y); } else { return CellData(); } }
    CellData atPos(Offset p) { return at((p.dx / cellWidth_).floor(), (p.dy / cellWidth_).floor()); }

    Coord pToCoord(Offset p) { return Coord((p.dx / cellWidth_).floor(), (p.dy / cellWidth_).floor()); }

    void addRowTop() { ++h_; cells_.insert(0, []); for(int i = 0; i < w_; ++i) { cells_[0].add(CellData()); } }
    void subRowTop() { --h_; cells_.removeAt(0); }
    void addRowBot() { ++h_; cells_.add([]); for(int i = 0; i < w_; ++i) { cells_.last.add(CellData()); } }
    void subRowBot() { --h_; cells_.removeAt(cells_.length - 1); }

    void addColLeft() { ++w_; for(int i = 0; i < h_; ++i) { cells_[i].insert(0, CellData()); } }
    void subColLeft() { --w_; for(int i = 0; i < h_; ++i) { cells_[i].removeAt(0); } }
    void addColRight() { ++w_; for(int i = 0; i < h_; ++i) { cells_[i].add(CellData()); } }
    void subColRight() { --w_; for(int i = 0; i < h_; ++i) { cells_[i].removeAt(cells_[i].length - 1); } }

    void fillGridWithPattern(bool Function(int, int) func) {
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                cells_[j][i].white = func(i, j);
    }

    //For saving/loading.
    void loadFromData(int w, int h, List<bool> white, List<String> chars) {
        w_ = w;
        h_ = h;

        cells_ = [];
        for(int j = 0; j < h_; ++j) {
            cells_.add([]);
            for(int i = 0; i < w_; ++i)
                cells_[j].add(CellData(white: white[i + (w_ * j)], c: chars[i + (w_ * j)]));
        }

        startFill();
    }

    List<bool> listWhite() {
        List<bool> oup = [];
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                oup.add(cells_[j][i].white);

        return oup;
    }

    List<String> listChars() {
        List<String> oup = [];
        for(int j = 0; j < h_; ++j)
            for(int i = 0; i < w_; ++i)
                oup.add(cells_[j][i].c);

        return oup;
    }
}
