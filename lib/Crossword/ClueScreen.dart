import "package:flutter/material.dart";
import "package:file_picker/file_picker.dart" as pfp;
import "dart:io" as dio;
import "dart:ui" as dui;

import "../Common/ToggleButton.dart";

import "ClueData.dart";
import "TextClue.dart";

class ClueScreenWidget extends StatefulWidget
{
    final ClueData clue;

    ClueScreenWidget ({ Key key, this.clue }) : super(key: key) {}
    ClueScreenWidgetState createState() { return ClueScreenWidgetState(clue); }
}

class ClueScreenWidgetState extends State<ClueScreenWidget>
{
    bool loadingFile_ = false;
    ClueData clue_;

    Offset holdDragStartPos_ = null;
    Offset dragStartPos_ = null;
    Offset dragEndPos_ = null;

    ClueScreenWidgetState (this.clue_) {
        if(clue_.snipPos != null && clue_.snipDim != null) {
            dragStartPos_ = clue_.snipPos;
            dragEndPos_ = Offset(clue_.snipPos.dx + clue_.snipDim.width, clue_.snipPos.dy + clue_.snipDim.height);
        }
    }

    Widget build(BuildContext ctx) {
        if(!clue_.isLoaded()) {
            return Column(children: [
                ToggleButtonWidget(Text("Load Clue Image"), 1.0, h: MediaQuery.of(ctx).size.height / 3.0, onPress: loadClues),
                ToggleButtonWidget(Text("Load Clue File"), 1.0, h: MediaQuery.of(ctx).size.height / 3.0, onPress: () { loadTextClues(ctx); })
            ]);
        }
        else {
            List<Widget> oup = [ Row(children: [
                ToggleButtonWidget(Text("Load New Image"), 0.5, h: 48.0, onPress: loadClues),
                ToggleButtonWidget(Text("Load New File"), 0.5, h: 48.0, onPress: () { loadTextClues(ctx); }),
            ]) ];

            if(!clue_.isText()) {
                oup.add(CheckboxListTile(
                    value: clue_.canSnip, onChanged: (bool newVal) { setState(() { clue_.setCanSnip(newVal); }); },
                    title: Text("Enable Snips")
                ));
            }

            if(clue_.isSnip())
                oup.add(ToggleButtonWidget(Text("Clear Selection"), 1.0, h: 48.0, onPress: clearSnip));

            if(clue_.isText()) {
                for(int i = 0; i < clue_.textData.length; ++i) {
                    if(i < clue_.textData.length - 1 && !clue_.textData[i].hoz && clue_.textData[i - 1].hoz)
                        oup.add(Divider(thickness: 2.0));
                    else
                        oup.add(Divider());

                    oup.add(TextClueWidget(
                        data: clue_.textData[i],
                        onSelect: () { clue_.selectTextClue(i); setState(() {}); },
                        onDeselect: () { clue_.selectTextClue(-1); setState(() {}); },
                    ));
                }
            }
            else if(clue_.canSnip) {
                oup.add(GestureDetector(
                    child: clueImg(ctx),
                    onTapDown: maybeStartDrag,
                    onPanStart: startDrag,
                    onPanUpdate: updateDrag,
                    onPanEnd: (DragEndDetails details) { stopDrag(details, ctx); }
                ));
            }
            else
                oup.add(clueImg(ctx));

            return ListView(children: oup);
        }
    }

    void loadClues() async {
        if(!loadingFile_) {
            loadingFile_ = true;
            dio.File newFile = await pfp.FilePicker.getFile(type: pfp.FileType.image);

            if(newFile != null) {
                clue_.file = newFile;
                clue_.textData = [];

                dui.Image decodedImage = await decodeImageFromList(clue_.file.readAsBytesSync());
                clue_.imgDim = Size(1.0 * decodedImage.width, 1.0 * decodedImage.height);
            }

            setState(() { loadingFile_ = false; });
        }
    }

    void loadTextClues(BuildContext ctx) async {
        if(!loadingFile_) {
            loadingFile_ = true;
            dio.File newFile = await pfp.FilePicker.getFile();

            if(newFile != null) {
                try {
                    //Read this first so that if it fails, the file doesn't get saved.
                    String fileText = await newFile.readAsString();

                    clue_.file = newFile;
                    clue_.readTextCluesFromString(fileText);
                }
                catch(e) {
                    Scaffold.of(ctx).showSnackBar(SnackBar(content: Text("Could not convert the file into text."), duration: Duration(seconds: 1)));
                }
            }

            setState(() { loadingFile_ = false; });
        }
    }

    Widget clueImg(BuildContext ctx) {
        return Container(
            width: MediaQuery.of(ctx).size.width, height: clue_.height(ctx),
            decoration: BoxDecoration(
                image: DecorationImage(image: FileImage(clue_.file), fit: BoxFit.fitWidth),
                border: genSnipOverlay(ctx)
            )
        );
    }

    void clearSnip() {
        clue_.clearSnip();
        dragStartPos_ = null;
        dragEndPos_ = null;

        setState(() {});
    }

    void maybeStartDrag(TapDownDetails details) { holdDragStartPos_ = details.localPosition; }

    void startDrag(DragStartDetails details) {
        if(holdDragStartPos_ == null)
            setState(() { dragStartPos_ = details.localPosition; dragEndPos_ = details.localPosition; });
        else
            setState(() { dragStartPos_ = holdDragStartPos_; dragEndPos_ = details.localPosition; holdDragStartPos_ = null; });
    }

    void updateDrag(DragUpdateDetails details) { setState(() { dragEndPos_ = details.localPosition; }); }

    void stopDrag(DragEndDetails details, BuildContext ctx) {
        clue_.setSnip(Offset(snip_l(0), snip_u(0)), Size(snip_r(MediaQuery.of(ctx).size.width) - snip_l(0), snip_d(clue_.height(ctx)) - snip_u(0)));
        setState(() {});
    }

    double snip_u(double min) { double oup = ((dragStartPos_.dy < dragEndPos_.dy) ? dragStartPos_.dy : dragEndPos_.dy); return ((oup < min) ? min : oup); }
    double snip_r(double max) { double oup = ((dragStartPos_.dx < dragEndPos_.dx) ? dragEndPos_.dx : dragStartPos_.dx); return ((oup > max) ? max : oup); }
    double snip_d(double max) { double oup = ((dragStartPos_.dy < dragEndPos_.dy) ? dragEndPos_.dy : dragStartPos_.dy); return ((oup > max) ? max : oup); }
    double snip_l(double min) { double oup = ((dragStartPos_.dx < dragEndPos_.dx) ? dragStartPos_.dx : dragEndPos_.dx); return ((oup < min) ? min : oup); }

    Border genSnipOverlay(BuildContext ctx) {
        if(dragStartPos_ == null || dragEndPos_ == null || !clue_.canSnip)
            return Border();
        else {
            return Border(
                top: BorderSide(color: Color(0x88000000), width: snip_u(0)),
                bottom: BorderSide(color: Color(0x88000000), width: clue_.height(ctx) - snip_d(clue_.height(ctx))),
                left: BorderSide(color: Color(0x88000000), width: snip_l(0)),
                right: BorderSide(color: Color(0x88000000), width: MediaQuery.of(ctx).size.width - snip_r(MediaQuery.of(ctx).size.width))
            );
        }
    }
}
