import "package:flutter/material.dart";
import "dart:io" as dio;
import "dart:ui" as dui;

import "../Common/Settings.dart";

import "TextClue.dart";

class ClueData
{
    void Function() redrawFunc = null;

    dio.File file = null;
    Size imgDim = null;

    bool canSnip = false;
    Offset snipPos = null;
    Size snipDim = null;

    List<TextClueData> textData = [];
    int selectedTextClueIndex = -1;

    ClueData () {}

    bool isLoaded() { return ((file != null && imgDim != null) || textData.length > 0); }
    bool isSnip() { return (canSnip && ((snipPos != null && snipDim != null) || (textData.length > 0 && selectedTextClueIndex >= 0))); }
    bool isText() { return (textData.length > 0); }

    TextClueData selectedTextClue() { return textData[selectedTextClueIndex]; }

    void setCanSnip(bool newVal) {
        canSnip = newVal;
        Settings.setBool(ThingToSave.Snip, newVal);

        if(redrawFunc != null)
            redrawFunc();
    }

    void setSnip(Offset tl, Size dim) {
        snipPos = tl;
        snipDim = dim;

        if(redrawFunc != null)
            redrawFunc();
    }

    void selectTextClue(int index) {
        if(isText()) {
            for(int i = 0; i < textData.length; ++i)
                if(i != index)
                    textData[i].selected = false;

            selectedTextClueIndex = index;

            if(redrawFunc != null)
                redrawFunc();
        }
    }

    void selectTextClueFromGrid(int num, bool hoz) {
        if(isText()) {
            int newIndex = -1;

            for(int i = 0; newIndex < 0 && i < textData.length; ++i)
                if(textData[i].num == num && textData[i].hoz == hoz)
                    newIndex = i;

            selectTextClue(newIndex);
        }
    }

    void clearSnip() {
        snipPos = null;
        snipDim = null;

        if(isText())
            selectTextClue(-1);
        else if(redrawFunc != null)
            redrawFunc();
    }

    String filePath() { return ((file == null) ? "" : file.path); }

    FractionalOffset snipToPos(BuildContext ctx) { return FractionalOffset(snipPos.dx / (MediaQuery.of(ctx).size.width - snipDim.width), snipPos.dy / (height(ctx) - snipDim.height)); }

    double height(BuildContext ctx) { return imgDim.height * MediaQuery.of(ctx).size.width / imgDim.width; }

    void readTextCluesFromString(String s) {
        canSnip = true;
        textData = [];

        String lineText = "";
        for(int i = 0; i <= s.length; ++i) {
            if(i == s.length || s[i] == "\n") {
                if(lineText.isNotEmpty) {
                    int colonPos = lineText.indexOf(":");
                    String numStr = "";
                    for(int j = 0; j < colonPos; ++j) {
                        if(int.tryParse(lineText[j]) == null)
                            break;
                        else
                            numStr += lineText[j];
                    }

                    textData.add(TextClueData(question: lineText.substring(colonPos + 1).trim(), num: int.parse(numStr), hoz: (lineText.substring(numStr.length, colonPos).trim() == "A")));
                    lineText = "";
                }
            }
            else
                lineText += s[i];
        }

        //Sort the clues.
        textData.sort((TextClueData a, TextClueData b) {
            if(a.hoz && !b.hoz || (a.hoz == b.hoz && a.num < b.num))
                return -1;
            else
                return 1;
        });
    }

    void loadFromData(String filePath, bool isTextFile, void Function() onFail) async {
        if(filePath != "") {
            file = dio.File(filePath);
            if(file != null) {
                if(isTextFile)
                    readTextCluesFromString(await file.readAsString());
                else {
                    dui.Image decodedImage = await decodeImageFromList(file.readAsBytesSync());
                    imgDim = Size(1.0 * decodedImage.width, 1.0 * decodedImage.height);
                }
            }
            else
                onFail();
        }

        clearSnip();
    }
}
