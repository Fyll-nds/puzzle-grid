import "package:flutter/material.dart";
import "package:url_launcher/url_launcher.dart";

import "Common/Settings.dart";
import "Common/AutoSaveLoad.dart";

import "Battleships/Battleships.dart";
import "Battleships/BattleshipsData.dart";

import "Crossword/Crossword.dart";
import "Crossword/CrosswordData.dart";

import "Sudoku/Sudoku.dart";
import "Sudoku/SudokuData.dart";

import "PuzzleButton.dart";

void main() { runApp(MyApp()); }

class MyApp extends StatelessWidget
{
    Widget build(BuildContext context) {
        return MaterialApp(
            title: "Puzzle Grid",
            theme: ThemeData(primarySwatch: Colors.grey),
            home: MainWidget()
        );
    }
}

class MainWidget extends StatefulWidget
{
    MainWidget({ Key key }) : super(key: key) {}
    MainWidgetState createState() { return MainWidgetState(); }
}

class MainWidgetState extends State<MainWidget>
{
    BattleshipsData battleships_ = BattleshipsData();
    CrosswordData crossword_ = CrosswordData();
    SudokuData sudoku_ = SudokuData();

    MainWidgetState () {
        Settings.addBool(ThingToSave.Autosave, false, (bool newVal) { setState(() { AutoSaveLoad.on = newVal; }); });
    }

    @override Widget build(BuildContext ctx) {
        if(Settings.finishedLoading) {
            return SafeArea(child: Scaffold(body: ListView(children: [
                genPuzzleEntry(ctx, "Battleships", (BuildContext) { return BattleshipsWidget(battleships_); }),
                genPuzzleEntry(ctx, "Crossword", (BuildContext) { return CrosswordWidget(crossword_); }),
                genPuzzleEntry(ctx, "Sudoku", (BuildContext) { return SudokuWidget(sudoku_); }),
                Divider(),
                ListTile(
                    title: Center(child: Text("Written by Iain Findlay")),
                    subtitle: Center(child: Text("Source Code (BitBucket)", style: TextStyle(color: Colors.blue, decoration: TextDecoration.underline))),
                    onTap: () { launch("https://bitbucket.org/fyll-nds/crossword-grid"); }
                )
            ])));
        }
        else
            return Container();
    }

    Widget genPuzzleEntry(BuildContext ctx, String name, Widget Function(BuildContext) genFunc) {
        return PuzzleButtonWidget("gfx/Button_" + name + ".png", name, () { Navigator.push(ctx, MaterialPageRoute(builder: genFunc)); });
    }
}
