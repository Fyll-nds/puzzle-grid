import "package:flutter/material.dart";

class ToggleButtonWidget extends StatefulWidget
{
    final Widget lbl;
    final double w;
    final double h;

    final bool Function() isPressed;
    final void Function() onPress;
    final void Function() onLongPress;

    ToggleButtonWidget (this.lbl, this.w, { this.h = null, this.isPressed = null, this.onPress = null, this.onLongPress = null }) : super(key: UniqueKey()) {}
    ToggleButtonWidgetState createState() { return ToggleButtonWidgetState(lbl, w, h, isPressed, onPress, onLongPress); }
}

class ToggleButtonWidgetState extends State<ToggleButtonWidget> with SingleTickerProviderStateMixin
{
    Widget lbl_;
    double w_;
    double h_;

    bool Function() isPressedFunc;
    void Function() onPressFunc;
    void Function() onLongPressFunc;

    AnimationController animCtrl_;
    Animation<double> anim_;

    ToggleButtonWidgetState (this.lbl_, this.w_, this.h_, this.isPressedFunc, this.onPressFunc, this.onLongPressFunc) {}

    @override void dispose() {
        animCtrl_.dispose();
        super.dispose();
    }

    @override void initState() {
        animCtrl_ = AnimationController(lowerBound: 0.0, upperBound: 1.0, duration: Duration(milliseconds: 640), vsync: this);
        anim_ = Tween<double>(begin: 1.0, end: 0.0).animate(animCtrl_);
        anim_.addListener(() { setState(() {}); });

        super.initState();
    }

    @override Widget build(BuildContext ctx) {
        //If it's been held for long enough, call the function.
        if(onLongPressFunc != null && animCtrl_.status == AnimationStatus.completed)
            WidgetsBinding.instance.addPostFrameCallback((_) { onLongPressFunc(); animCtrl_.reset(); });

        bool isPressed = (isPressedFunc != null && isPressedFunc());
        Color col = (isPressed ? Color(0xffb0b0b0) : Color(0xffe0e0e0));
        if(anim_.status == AnimationStatus.forward || anim_.status == AnimationStatus.completed) {
            double colScale = (anim_.value + 1.0) / 2.0;
            col = Color.fromARGB(col.alpha, (col.red * colScale).floor(), (col.green * colScale).floor(), (col.blue * colScale).floor());
        }

        return Container(
            width: MediaQuery.of(ctx).size.width * w_,
            height: h_,
            padding: EdgeInsets.all(4.0),
            child: Container(
                decoration: BoxDecoration(
                    color: col,
                    border: (isPressed ? Border.all(width: 2.0) : null),
                    borderRadius: BorderRadius.circular(4.0),
                    boxShadow: (isPressed ? [] : [ BoxShadow(offset: Offset(1.0, 1.0), blurRadius: 1.0) ])
                ),
                padding: EdgeInsets.all(isPressed ? 2.0 : 4.0),
                child: GestureDetector(
                    child: Container(
                        color: Colors.transparent,
                        child: Align(child: lbl_)
                    ),
                    onTapUp: (_) {
                        if(onPressFunc != null) {
                            onPressFunc();
                            setState(() {});
                        }

                        cancelAnimation();
                    },
                    onTapDown: (_) {
                        if(onLongPressFunc != null)
                            animCtrl_.forward();
                    },
                    onLongPressMoveUpdate: (LongPressMoveUpdateDetails details) {
                        RenderBox box = ctx.findRenderObject();
                        if(details.localPosition.dx < 0 || details.localPosition.dy < 0 || details.localPosition.dx >= box.size.width || details.localPosition.dy >= box.size.height)
                            cancelAnimation();
                    },
                    onLongPressUp: cancelAnimation
                )
            )
        );
    }

    void cancelAnimation() {
        if(onLongPressFunc != null && animCtrl_.status != AnimationStatus.dismissed)
            animCtrl_.reset();
    }
}
