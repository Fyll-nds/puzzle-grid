import "dart:io" as dio;
import "package:path_provider/path_provider.dart";
import "dart:convert" as dcn;
import "dart:collection" as dco;

const String BATTLESHIPS_SAVE_FILE_NAME = "battleships_saves.txt";
const String CROSSWORD_SAVE_FILE_NAME = "crossword_saves.txt";
const String SUDOKU_SAVE_FILE_NAME = "sudoku_saves.txt";

class DataToSave
{
    String fileName; String name;
    Map<String, dynamic> data;
    void Function() thenDoFunc;

    DataToSave (this.fileName, this.name, this.data, this.thenDoFunc) {}
}

class SaveLoad
{
    static Map<String, dynamic> deJSON_Map(String json) { return ((json.length > 0) ? dcn.jsonDecode(json) : Map.fromEntries([])); }

    static Future<String> get localPath_ async { return (await getApplicationDocumentsDirectory()).path; }
    static Future<dio.File> localFile(String fileName) async { return dio.File((await localPath_).toString() + "/" + fileName); }

    static Future<dio.File> write(String fileName, String txt) async { return (await localFile(fileName)).writeAsString(txt); }

    static Future<String> read(String fileName) async {
        try { return await (await localFile(fileName)).readAsString(); }
        catch(e) { return ""; }
    }

    static dco.Queue<DataToSave> saveQueue_ = dco.Queue<DataToSave>();

    static void save(String fileName, String name, Map<String, dynamic> data, [ void Function() thenDoFunc = null ])
    {
        saveQueue_.addLast(DataToSave(fileName, name, data, thenDoFunc));
        if(saveQueue_.length == 1)
            saveNextInQueue();
    }

    static void saveNextInQueue()
    {
        read(saveQueue_.first.fileName).then((String fileText) {
            Map<String, dynamic> allData;
            if(fileText == null)
                allData = Map<String, dynamic>.fromEntries([]);
            else
                allData = deJSON_Map(fileText);

            //If this data hasn't been saved before, make a space for it.
            if(!allData.keys.contains(saveQueue_.first.name))
                allData.addEntries([ MapEntry(saveQueue_.first.name, Map<String, dynamic>.fromEntries([])) ]);

            Map<String, dynamic> data = allData[saveQueue_.first.name];

            for(MapEntry<String, dynamic> entry in saveQueue_.first.data.entries)
                data = saveEntry(data, entry.key, entry.value);

            allData[saveQueue_.first.name] = data;
            write(saveQueue_.first.fileName, dcn.jsonEncode(allData)).then((String) {
                if(saveQueue_.first.thenDoFunc != null)
                    saveQueue_.first.thenDoFunc();

                saveQueue_.removeFirst();
                if(saveQueue_.isNotEmpty)
                    saveNextInQueue();
            });
        });
    }

    static Map<String, dynamic> saveEntry(Map<String, dynamic> data, String key, dynamic val) {
        if(!data.keys.contains(key))
            data.addEntries([MapEntry(key, val)]);
        else
            data[key] = val;

        return data;
    }

    static Future< List<String> > loadOpts(String fileName) async { return List<String>.from(deJSON_Map(await read(fileName)).keys); }

    static void load(String fileName, String name, void Function(Map<String, dynamic>) callOnFinish) {
        read(fileName).then((String text) {
            if(text == null)
                callOnFinish(null);
            else {
                Map<String, dynamic> allData = deJSON_Map(text);

                if(allData.keys.contains(name) && allData[name].length > 0)
                    callOnFinish(allData[name]);
                else
                    callOnFinish(null);
            }
        });
    }

    static void delete(String fileName, String name, [ void Function() thenDoFunc = null ]) {
        read(fileName).then((String fileText) {
            Map<String, dynamic> allData = deJSON_Map(fileText);
            allData.remove(name);

            if(thenDoFunc == null)
                write(fileName, dcn.jsonEncode(allData));
            else
                write(fileName, dcn.jsonEncode(allData)).then((String) { thenDoFunc(); });
        });
    }
}
