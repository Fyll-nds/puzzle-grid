import "package:flutter/material.dart";

class NumberSelectWidget extends StatefulWidget
{
    final int startingValue;
    final void Function(int) onChangeFunc;

    final double Function() width;
    final double Function() height;

    final double margin;
    final bool disabled;
    final TextStyle Function() styleFunc;

    final bool withBlank;
    final bool withZero;
    final bool withType;

    NumberSelectWidget (
        this.startingValue, this.onChangeFunc,
        {
            Key key,
            this.width = null, this.height = null,
            this.margin = 4.0, this.disabled = false, this.styleFunc = null,
            this.withBlank = false, this.withZero = true, this.withType = true
        }
    ) : super(key: key) {}
    NumberSelectWidgetState createState() { return NumberSelectWidgetState(startingValue, onChangeFunc, width, height, margin, disabled, styleFunc, withBlank, withZero, withType); }
}

class NumberSelectWidgetState extends State<NumberSelectWidget> with SingleTickerProviderStateMixin
{
    int value_;
    void Function(int) onChangeFunc;

    double Function() wFunc;
    double Function() hFunc;

    double margin_;
    bool disabled_;
    TextStyle Function() styleFunc;

    bool withBlank_;
    bool withZero_;
    bool withType_;

    double get fullHeight { return (3.0 + (withZero_ ? 1.0 : 0.0) + (withBlank_ ? 1.0 : 0.0) + (withType_ ? 1.0 : 0.0)); }

    AnimationController anim_;
    OverlayEntry overlay_;
    GlobalKey key_;

    FocusNode inpFocus_;

    NumberSelectWidgetState (this.value_, this.onChangeFunc, this.wFunc, this.hFunc, this.margin_, this.disabled_, this.styleFunc, this.withBlank_, this.withZero_, this.withType_) {}

    @override void dispose() {
        if(overlay_ != null)
            overlay_.remove();

        anim_.dispose();
        inpFocus_.dispose();

        super.dispose();
    }

    @override void initState() {
        anim_ = AnimationController(lowerBound: 0.0, upperBound: 1.0, duration: Duration(milliseconds: 160), vsync: this);
        key_ = GlobalKey();

        inpFocus_ = FocusNode();

        super.initState();
    }

    @override Widget build(BuildContext ctx) {
        if(wFunc == null || hFunc == null) {
            List<Widget> lst = [ Container(child: alignedNumWidget()) ];

            if(!disabled_ && anim_.status == AnimationStatus.dismissed) {
                lst.add(GestureDetector(
                    key: key_,
                    onTapDown: (_) { tapDown(ctx); },
                    onPanStart: (_) {},
                    child: Container(child: alignedNumWidget())
                ));
            }

            return Stack(children: lst);
        }
        else {
            Size dim = Size(wFunc(), hFunc());
            Size inDim = Size((dim.width / 2.0) - (2.0 * margin_), (dim.height / 2.0) - (2.0 * margin_));

            List<Widget> lst = [
                Container(
                    width: dim.width, height: dim.height,
                    child: alignedNumWidget()
                ),
                (disabled_
                    ? Container()
                    : Column(children: [
                        Row(children: [
                            Container(
                                width: inDim.width, height: inDim.height, margin: EdgeInsets.all(margin_),
                                decoration: BoxDecoration(border: Border(top: BorderSide(), left: BorderSide()))
                            ),
                            Container(
                                width: inDim.width, height: inDim.height, margin: EdgeInsets.all(margin_),
                                decoration: BoxDecoration(border: Border(top: BorderSide(), right: BorderSide()))
                            )
                        ]),
                        Row(children: [
                            Container(
                                width: inDim.width, height: inDim.height, margin: EdgeInsets.all(margin_),
                                decoration: BoxDecoration(border: Border(bottom: BorderSide(), left: BorderSide()))
                            ),
                            Container(
                                width: inDim.width, height: inDim.height, margin: EdgeInsets.all(margin_),
                                decoration: BoxDecoration(border: Border(bottom: BorderSide(), right: BorderSide()))
                            )
                        ])
                    ])
                )
            ];

            if(!disabled_ && anim_.status == AnimationStatus.dismissed) {
                lst.add(Container(
                    width: dim.width, height: dim.height,
                    child: GestureDetector(
                        key: key_,
                        onTapDown: (_) { tapDown(ctx); },
                        onPanStart: (_) {}
                    )
                ));
            }

            return Stack(children: lst);
        }
    }

    Widget alignedNumWidget() { return Align(child: Text(((value_ == -1) ? "" : value_.toString()), style: ((styleFunc == null) ? null : styleFunc()))); }

    void tapDown(BuildContext ctx) {
        if(anim_.status == AnimationStatus.dismissed) {
            RenderBox box = ctx.findRenderObject();
            Offset pos = box.localToGlobal(Offset.zero);
            Size fullDim = Size(3.0 * box.size.width, fullHeight * box.size.height);

            //Pop up the pop up bit.
            overlay_ = OverlayEntry(
                builder: (BuildContext overlayCtx) {
                    Size screenDim = MediaQuery.of(ctx).size;

                    //Make sure the pop-up stays on screen.
                    Offset finalPos = Offset(pos.dx - box.size.width, pos.dy - (fullDim.height - box.size.height) / 2.0);
                    if(finalPos.dx < 0)
                        finalPos = Offset(0.0, finalPos.dy);
                    else if(finalPos.dx + fullDim.width >= screenDim.width)
                        finalPos = Offset(screenDim.width - fullDim.width, finalPos.dy);
                    if(finalPos.dy < 0)
                        finalPos = Offset(finalPos.dx, 0.0);
                    else if(finalPos.dy + fullDim.height >= screenDim.height)
                        finalPos = Offset(finalPos.dx, screenDim.height - fullDim.height);

                    List<Widget> rows = [];

                    if(withBlank_) {
                        rows.add(Container(
                            width: fullDim.width, height: box.size.height,
                            color: ((value_ == -1) ? Colors.grey : null),
                            child: Align(child: Text("Blank", style: TextStyle(color: Colors.grey)))
                        ));
                    }

                    if(withZero_)
                        rows.add(genNumber(0, fullDim, 1.0));

                    rows.add(Row(children: [ genNumber(1, fullDim, 3.0), genNumber(2, fullDim, 3.0), genNumber(3, fullDim, 3.0) ]));
                    rows.add(Row(children: [ genNumber(4, fullDim, 3.0), genNumber(5, fullDim, 3.0), genNumber(6, fullDim, 3.0) ]));
                    rows.add(Row(children: [ genNumber(7, fullDim, 3.0), genNumber(8, fullDim, 3.0), genNumber(9, fullDim, 3.0) ]));

                    if(withType_) {
                        rows.add(Stack(children: [
                            Container(
                                width: fullDim.width, height: box.size.height,
                                color: ((value_ == null) ? Colors.grey : null),
                                child: Align(child: Text("Type", style: TextStyle(color: Colors.grey)))
                            ),
                            Container(
                                width: fullDim.width, height: box.size.height,
                                child: Align(child: TextField(
                                    focusNode: inpFocus_,
                                    decoration: null,
                                    keyboardType: TextInputType.number,
                                    textAlign: TextAlign.center, textAlignVertical: TextAlignVertical(y: 0.0),
                                    onSubmitted: (String newVal) { value_ = int.parse(newVal); tapUp(); }
                                ))
                            )
                        ]));
                    }

                    return Stack(children: [
                        PositionedTransition(
                            rect: anim_.drive(RelativeRectTween(
                                begin: RelativeRect.fromRect(pos & box.size, Offset.zero & screenDim),
                                end: RelativeRect.fromRect(finalPos & fullDim, Offset.zero & screenDim)
                            )),
                            child: Material(
                                elevation: 4.0,
                                child: ClipRect(child: OverflowBox(
                                    maxWidth: fullDim.width, minWidth: 0.0, maxHeight: fullDim.height, minHeight: 0.0,
                                    child: Column(children: rows)
                                ))
                            )
                        ),
                        GestureDetector(
                            key: key_,
                            child: Container(width: screenDim.width, height: screenDim.height, color: Color(0x00ffffff)),
                            onTapUp: (TapUpDetails details) { drag(details.globalPosition, ctx); tapUp(); },
                            onPanStart: (_) {},
                            onPanEnd: (DragEndDetails details) { tapUp(); },
                            onPanUpdate: (DragUpdateDetails details) { drag(details.globalPosition, ctx); },
                            onVerticalDragStart: (_) {},
                            onVerticalDragEnd: (DragEndDetails details) { tapUp(); },
                            onVerticalDragUpdate: (DragUpdateDetails details) { drag(details.globalPosition, ctx); },
                        )
                    ]);
                }
            );

            anim_.forward();
            setState(() {});
            Overlay.of(ctx).insert(overlay_);
        }
    }

    Widget genNumber(int val, Size boxDim, double widthFactor) {
        return Container(
            width: boxDim.width / widthFactor, height: boxDim.height / fullHeight,
            child: Align(child: Text((val >= 0) ? val.toString() : "")),
            color: ((val == value_) ? Colors.grey : null)
        );
    }

    void drag(Offset pos, BuildContext ctx) {
        if(anim_.status == AnimationStatus.forward || anim_.status == AnimationStatus.completed) {
            RenderBox box = ctx.findRenderObject();
            Size screenDim = MediaQuery.of(ctx).size;
            Rect drawBox = (box.localToGlobal(Offset.zero) - Offset(box.size.width, (fullHeight - 1.0) * box.size.height / 2.0)) & Size(3.0 * box.size.width, fullHeight * box.size.height);

            if(drawBox.left < 0)
                drawBox = Offset(0.0, drawBox.top) & drawBox.size;
            else if(drawBox.right >= screenDim.width)
                drawBox = Offset(screenDim.width - drawBox.width, drawBox.top) & drawBox.size;
            if(drawBox.top < 0)
                drawBox = Offset(drawBox.left, 0.0) & drawBox.size;
            else if(drawBox.bottom >= screenDim.height)
                drawBox = Offset(drawBox.left, screenDim.height - drawBox.height) & drawBox.size;

            Offset inBoxPos = pos - drawBox.topLeft;

            if(inBoxPos.dx >= 0.0 && inBoxPos.dy >= 0.0 && inBoxPos.dx < drawBox.width && inBoxPos.dy < drawBox.height) {
                if(withBlank_) {
                    if(inBoxPos.dy < box.size.height)
                        value_ = -1;

                    inBoxPos = inBoxPos.translate(0.0, -1.0 * box.size.height);
                }

                if(withZero_) {
                    if(inBoxPos.dy >= 0.0 && inBoxPos.dy < box.size.height)
                        value_ = 0;

                    inBoxPos = inBoxPos.translate(0.0, -1.0 * box.size.height);
                }

                if(inBoxPos.dy >= 0.0) {
                    if(inBoxPos.dy < box.size.height)
                        value_ = (inBoxPos.dx ~/ box.size.width) + 1;
                    else if(inBoxPos.dy < 2.0 * box.size.height)
                        value_ = (inBoxPos.dx ~/ box.size.width) + 4;
                    else if(inBoxPos.dy < 3.0 * box.size.height)
                        value_ = (inBoxPos.dx ~/ box.size.width) + 7;
                }

                inBoxPos = inBoxPos.translate(0.0, -3.0 * box.size.height);

                if(withType_) {
                    if(inBoxPos.dy >= 0.0 && inBoxPos.dy < box.size.height)
                        value_ = null;

                    inBoxPos = inBoxPos.translate(0.0, -1.0 * box.size.height);
                }

                setState(() {});
                overlay_.markNeedsBuild();
            }
        }
    }

    void tapUp() {
        if(anim_.status == AnimationStatus.completed) {
            if(value_ == null)
                inpFocus_.requestFocus();
            else {
                if(onChangeFunc != null)
                    onChangeFunc(value_);

                anim_.reverse().then((_) { overlay_.remove(); overlay_ = null; setState(() {}); });
            }
        }
    }
}
