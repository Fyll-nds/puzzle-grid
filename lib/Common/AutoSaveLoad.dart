import "../Puzzles.dart";

import "SaveLoad.dart";
import "Settings.dart";

const String AUTO_SAVE_FILE_NAME = "autosave.txt";

class AutoSaveLoad
{
    static bool on = false;
    static bool finishedLoading = false;

    static Map<Puzzle, void Function(String, String)> saveFuncs = Map<Puzzle, void Function(String, String)>();

    AutoSaveLoad () {}

    static void setSaveFunc(Puzzle src, void Function(String, String) func) {
        if(saveFuncs.containsKey(src))
            saveFuncs[src] = func;
        else
            saveFuncs.addEntries([ MapEntry<Puzzle, void Function(String, String)>(src, func) ]);
    }

    static void setOn(bool newVal) {
        if(newVal != on) {
            on = newVal;
            Settings.setBool(ThingToSave.Autosave, newVal);
        }
    }

    static void load(Puzzle src, void Function(Map<String, dynamic>) loadFunc, void Function() callOnFinish) {
        finishedLoading = false;

        SaveLoad.read(AUTO_SAVE_FILE_NAME).then((String fileText) {
            if(fileText != null) {
                Map<String, dynamic> data = SaveLoad.deJSON_Map(fileText);

                if(on && data.containsKey(src.toString())) {
                    SaveLoad.load(AUTO_SAVE_FILE_NAME, src.toString(), (Map<String, dynamic> data) {
                        loadFunc(data);
                        finishedLoading = true;
                        callOnFinish();
                    });
                }
                else {
                    finishedLoading = true;
                    callOnFinish();
                }
            }
            else {
                finishedLoading = true;
                callOnFinish();
            }
        });
    }

    static void save(Puzzle src) {
        if(on)
            saveFuncs[src](AUTO_SAVE_FILE_NAME, src.toString());
    }
}