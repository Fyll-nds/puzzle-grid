import "package:flutter/material.dart";

import "../Common/Settings.dart";
import "../Common/AutoSaveLoad.dart";
import "../Common/SaveLoadScreen.dart";

abstract class PuzzleWidgetState<T extends StatefulWidget> extends State<T> with SingleTickerProviderStateMixin
{
    List<Tab> tabs_;
    TabController tabAnimCtrl_;

    bool hasChangedTab_ = false;
    int extraTabs_ = 1;

    PuzzleWidgetState () {}

    void initTabs(List<String> lbls, { bool withGen = false }) {
        extraTabs_ = 1;
        if(withGen)
            ++extraTabs_;

        tabs_ = List<Tab>.generate(lbls.length + extraTabs_, (int index) {
            if(index < lbls.length)
                return Tab(text: lbls[index]);
            else if(withGen && index == lbls.length)
                return Tab(icon: Icon(Icons.brush));
            else
                return Tab(icon: Icon(Icons.save));
        });

        tabAnimCtrl_ = TabController(length: tabs_.length, vsync: this);
    }

    @override void initState() { tabAnimCtrl_.addListener(() { setState(() { hasChangedTab_ = true; }); }); super.initState(); }
    @override void dispose() { tabAnimCtrl_.dispose(); super.dispose(); }

    @override Widget build(BuildContext ctx) {
        if(Settings.finishedLoading && AutoSaveLoad.finishedLoading) {
            if(hasChangedTab_) {
                //Unfocus when you change tabs.
                FocusScope.of(ctx).requestFocus(new FocusNode());
                hasChangedTab_ = false;

                onChangeTab();
            }

            double tabWidth = MediaQuery.of(ctx).size.width / ((2 * tabs_.length) - extraTabs_);

            return SafeArea(child: Scaffold(body: Column(children: [
                topWidget(),
                TabBar(
                    controller: tabAnimCtrl_,
                    isScrollable: true,
                    labelPadding: EdgeInsets.all(0.0),
                    tabs: List<Widget>.generate(tabs_.length, (int index) {
                        if(index < tabs_.length - extraTabs_)
                            return Container(width: 2.0 * tabWidth, child: tabs_[index]);
                        else
                            return Container(width: tabWidth, child: tabs_[index]);
                    })
                ),
                Container(width: 1, height: 8),
                Expanded(child: TabBarView(
                    controller: tabAnimCtrl_,
                    physics: NeverScrollableScrollPhysics(),
                    children: tabWidgets()
                ))
            ])));
        }
        else
            return Container();
    }

    void onChangeTab() {}

    void loadFromData(Map<String, dynamic> data);
    void save(String fileName, String key);

    Widget topWidget() { return Container(); }
    List<Widget> tabWidgets();
}
