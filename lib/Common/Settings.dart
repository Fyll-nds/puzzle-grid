import "SaveLoad.dart";

const String SETTINGS_FILE_NAME = "settings.txt";

enum ThingToSave {
    Autosave, Snip,
    Symmetry
}

class Settings
{
    static Map<ThingToSave, bool> bools = Map<ThingToSave, bool>();
    static Map<ThingToSave, void Function(bool)> onBoolChange = Map<ThingToSave, void Function(bool)>();

    static Map<ThingToSave, int> ints = Map<ThingToSave, int>();
    static Map<ThingToSave, void Function(int)> onIntChange = Map<ThingToSave, void Function(int)>();

    static int varsInitialised = 0;
    static bool get finishedLoading { return (varsInitialised == bools.length + ints.length); }

    Settings () {}

    static void addBool(ThingToSave key, bool defaultVal, void Function(bool) onChange) {
        if(!bools.containsKey(key)) {
            bools.addEntries([ MapEntry<ThingToSave, bool>(key, defaultVal) ]);
            onBoolChange.addEntries([ MapEntry<ThingToSave, void Function(bool)>(key, onChange) ]);

            loadBool(key);
        }
    }

    static void addInt(ThingToSave key, int defaultVal, void Function(int) onChange) {
        if(!ints.containsKey(key)) {
            ints.addEntries([ MapEntry<ThingToSave, int>(key, defaultVal) ]);
            onIntChange.addEntries([ MapEntry<ThingToSave, void Function(int)>(key, onChange) ]);

            loadInt(key);
        }
    }

    static void setBool(ThingToSave key, bool newVal) {
        if(newVal != bools[key]) {
            bools[key] = newVal;
            SaveLoad.save(SETTINGS_FILE_NAME, key.toString(), Map<String, dynamic>.fromEntries([ MapEntry<String, dynamic>("val", newVal) ]));
        }
    }

    static void setInt(ThingToSave key, int newVal) {
        if(newVal != ints[key]) {
            ints[key] = newVal;
            SaveLoad.save(SETTINGS_FILE_NAME, key.toString(), Map<String, dynamic>.fromEntries([ MapEntry<String, dynamic>("val", newVal) ]));
        }
    }

    static void loadBool(ThingToSave key) {
        SaveLoad.read(SETTINGS_FILE_NAME).then((String fileText) {
            if(fileText != null) {
                Map<String, dynamic> data = SaveLoad.deJSON_Map(fileText);
                if(data.containsKey(key.toString())) {
                    SaveLoad.load(SETTINGS_FILE_NAME, key.toString(), (Map<String, dynamic> data) {
                        bools[key] = data["val"];

                        ++varsInitialised;
                        onBoolChange[key](bools[key]);
                    });
                    
                    return;
                }
            }
            
            ++varsInitialised;
            onBoolChange[key](bools[key]);
        });
    }

    static void loadInt(ThingToSave key) {
        SaveLoad.read(SETTINGS_FILE_NAME).then((String fileText) {
            if(fileText != null) {
                Map<String, dynamic> data = SaveLoad.deJSON_Map(fileText);
                if(data.containsKey(key.toString())) {
                    SaveLoad.load(SETTINGS_FILE_NAME, key.toString(), (Map<String, dynamic> data) {
                        ints[key] = data["val"];

                        ++varsInitialised;
                        onIntChange[key](ints[key]);
                    });

                    return;
                }
            }

            ++varsInitialised;
            onIntChange[key](ints[key]);
        });
    }
}