import "package:flutter/material.dart";

import "SaveLoad.dart";
import "AutoSaveLoad.dart";
import "ToggleButton.dart";

const double SAVELOAD_PAD = 16;

class SaveLoadScreenWidget extends StatefulWidget
{
    final void Function(String, String) saveFunc;
    final void Function(Map<String, dynamic>) loadFunc;
    final String fileName;

    SaveLoadScreenWidget ({ Key key, this.saveFunc = null, this.loadFunc = null, this.fileName }) : super(key: key) {}
    SaveLoadScreenWidgetState createState() { return SaveLoadScreenWidgetState(saveFunc, loadFunc, fileName); }
}

class SaveLoadScreenWidgetState extends State<SaveLoadScreenWidget>
{
    void Function(String, String) saveFunc;
    void Function(Map<String, dynamic>) loadFunc;
    String fileName_;

    bool finishedReading_ = false;
    String saveName_ = "";

    List< DropdownMenuItem<String> > loadList_ = [];
    String loadName_ = "";
    bool nothingToLoad_ = false;

    SaveLoadScreenWidgetState (this.saveFunc, this.loadFunc, this.fileName_) { readLoadList(noCallingSetState: true); }

    Widget build(BuildContext ctx) {
        return ListView(children: [
            Container(
                padding: EdgeInsets.all(SAVELOAD_PAD),
                child: TextField(
                    decoration: InputDecoration(labelText: defaultSaveName(), border: OutlineInputBorder()),
                    onChanged: (String txt) { setState(() { saveName_ = txt; }); },
                    onSubmitted: (_) { tryToSave(ctx); }
                )
            ),
            ToggleButtonWidget(Text("Save"), 1.0, h: 48.0, onPress: () { tryToSave(ctx); }),
            genDivider(),
            (nothingToLoad_
                ? Center(child: Text("Nothing to load!"))
                : Column(children: [
                    DropdownButton<String>(
                        value: loadName_, items: loadList_,
                        onChanged: (!finishedReading_ ? null : (String newVal) { setState(() { loadName_ = newVal; }); }),
                        disabledHint: Text("Reading save data....")
                    ),
                    ToggleButtonWidget(Text("Load"), 1.0, h: 48.0, onPress: () { tryToLoad(ctx); }),
                    ToggleButtonWidget(Text("Delete file (hold)"), 1.0, h: 48.0, onLongPress: () { tryToDelete(ctx); })
                ])
            ),
            genDivider(),
            CheckboxListTile(
                value: AutoSaveLoad.on, onChanged: (bool newVal) { setState(() { AutoSaveLoad.setOn(newVal); }); },
                title: Text("Auto-save/load"),
                subtitle: Text("When the app is opened, automatically re-open the grid that was in use when the app was last closed.")
            ),
            genDivider(6),
            ToggleButtonWidget(Text("Return to main menu."), 1.0, h: 48.0, onPress: () { Navigator.pop(ctx); })
        ]);
    }

    Widget genDivider([double thickness = 2]) { return Divider(height: 2.0 * SAVELOAD_PAD, thickness: thickness); }

    void popupSnack(BuildContext ctx, String msg) { Scaffold.of(ctx).showSnackBar(SnackBar(content: Text(msg), duration: Duration(seconds: 1))); }

    String defaultSaveName() { return DateTime.now().year.toString() + "-" + DateTime.now().month.toString() + "-" + DateTime.now().day.toString(); }

    void tryToSave(BuildContext ctx) {
        if(!finishedReading_)
            return;

        String key = saveName_;
        if(key == "")
            key = defaultSaveName();

        saveFunc(fileName_, key);
        popupSnack(ctx, "Saved Successfully!");

        addLoadOption(key);
        FocusScope.of(ctx).requestFocus(new FocusNode());

        setState(() {});
    }

    void tryToLoad(BuildContext ctx) {
        if(!finishedReading_)
            return;

        SaveLoad.load(fileName_, loadName_, (Map<String, dynamic> data) { parse(ctx, data); });
        FocusScope.of(ctx).requestFocus(new FocusNode());
    }

    void parse(BuildContext ctx, Map<String, dynamic> data) {
        if(data == null)
            popupSnack(ctx, "Failed to load " + loadName_ + ".");
        else {
            loadFunc(data);
            popupSnack(ctx, "Loaded Successfully!");
        }

        readLoadList();
    }

    void tryToDelete(BuildContext ctx) {
        if(!finishedReading_)
            return;

        SaveLoad.delete(fileName_, loadName_, readLoadList);
        popupSnack(ctx, "Deleted!");

        loadList_.remove(loadName_);
        if(loadList_.isEmpty)
            nothingToLoad_ = true;

        FocusScope.of(ctx).requestFocus(new FocusNode());
        setState(() {});
    }

    void readLoadList({ bool noCallingSetState = false }) {
        if(noCallingSetState)
            finishedReading_ = false;
        else
            setState(() { finishedReading_ = false; });

        loadList_ = [];
        loadName_ = "";
        nothingToLoad_ = true;

        SaveLoad.loadOpts(fileName_).then((List<String> opts) {
            for(int i = 0; i < opts.length; ++i)
                addLoadOption(opts[i]);

            setState(() { finishedReading_ = true; });
        });
    }

    void addLoadOption(String txt) {
        for(int i = 0; i < loadList_.length; ++i)
            if(loadList_[i].value == txt)
                loadList_.removeAt(i--);

        loadList_.add(DropdownMenuItem<String>(value: txt, child: Text(txt)));

        if(nothingToLoad_) {
            nothingToLoad_ = false;
            loadName_ = txt;
        }
    }
}
