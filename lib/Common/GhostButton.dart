import "package:flutter/material.dart";

class GhostButtonWidget extends StatefulWidget
{
    final double w;
    final bool startPressed;

    final void Function() onPress;
    final void Function(bool) onClear;
    final void Function() onKeep;

    final Widget Function(int) iconFill;

    GhostButtonWidget (this.w, {
        Key key = null, this.startPressed = false,
        this.onPress = null, this.onClear = null, this.onKeep = null,
        this.iconFill = null
    }) : super(key: key) {}
    GhostButtonWidgetState createState() { return GhostButtonWidgetState(w, startPressed, onPress, onClear, onKeep, iconFill); }
}

class GhostButtonWidgetState extends State<GhostButtonWidget> with TickerProviderStateMixin
{
    double w_;
    bool startPressed_;

    void Function() onPressFunc;
    void Function(bool) onClearFunc;
    void Function() onKeepFunc;

    Widget Function(int) iconFillFunc;

    AnimationController splitAnimCtrl_;
    Animation<double> splitAnim_;
    AnimationController fadeAnimCtrl_;
    Animation<double> fadeAnim_;
    bool get isUnpressed { return (splitAnimCtrl_.status == AnimationStatus.dismissed); }
    bool get isPressed { return (fadeAnimCtrl_.status == AnimationStatus.completed); }

    GhostButtonWidgetState (this.w_, this.startPressed_, this.onPressFunc, this.onClearFunc, this.onKeepFunc, this.iconFillFunc) {}

    @override void dispose() {
        splitAnimCtrl_.dispose();
        fadeAnimCtrl_.dispose();
        super.dispose();
    }

    @override void initState() {
        splitAnimCtrl_ = AnimationController(lowerBound: 0.0, upperBound: 1.0, duration: Duration(milliseconds: 256), vsync: this);
        splitAnim_ = Tween<double>(begin: 1.0, end: 0.0).animate(splitAnimCtrl_);
        splitAnim_.addListener(() { setState(() {}); });
        fadeAnimCtrl_ = AnimationController(lowerBound: 0.0, upperBound: 1.0, duration: Duration(milliseconds: 256), vsync: this);
        fadeAnim_ = Tween<double>(begin: 0.0, end: 1.0).animate(fadeAnimCtrl_);
        fadeAnim_.addListener(() { setState(() {}); });

        if(startPressed_) {
            splitAnimCtrl_.value = splitAnimCtrl_.upperBound;
            fadeAnimCtrl_.value = splitAnimCtrl_.upperBound;
        }

        super.initState();
    }

    @override Widget build(BuildContext ctx) {
        Size dim = Size(MediaQuery.of(ctx).size.width * w_, 48.0);

        return Container(
            width: dim.width,
            child: Row(children: [
                ClipRect(child: SizedOverflowBox(
                    size: Size(dim.width / 3.0, dim.height),
                    alignment: Alignment.centerLeft,
                    child: buildButton(ctx, leftButtonIcon(), dim.height, () { onClearFunc(false); return true; })
                )),
                ClipRect(child: SizedOverflowBox(
                    size: Size(dim.width / 3.0, dim.height),
                    alignment: Alignment.center,
                    child: buildButton(ctx, centralButtonIcon(), dim.height, () { onClearFunc(true); return false; })
                )),
                ClipRect(child: SizedOverflowBox(
                    size: Size(dim.width / 3.0, dim.height),
                    alignment: Alignment.centerRight,
                    child: buildButton(ctx, rightButtonIcon(), dim.height, () { onKeepFunc(); return true; })
                ))
            ])
        );
    }

    Widget buildButton(BuildContext ctx, Widget child, double height, bool Function() func) {
        return Container(
            width: MediaQuery.of(ctx).size.width * w_ * (1.0 + splitAnim_.value) / 3.0,
            height: height,
            padding: EdgeInsets.all(4.0),
            child: Container(
                decoration: BoxDecoration(
                    color: Color(0xffe0e0e0),
                    borderRadius: BorderRadius.circular(4.0),
                    boxShadow: [ BoxShadow(offset: Offset(1.0, 1.0), blurRadius: 1.0) ]
                ),
                padding: EdgeInsets.all(4.0),
                child: GestureDetector(
                    child: Container(
                        color: Colors.transparent,
                        child: Align(child: child)
                    ),
                    onTapUp: (_) {
                        if(isUnpressed) {
                            onPressFunc();
                            splitAnimCtrl_.forward().then((_) { fadeAnimCtrl_.forward(); });
                        }
                        else if(isPressed) {
                            if(func())
                                fadeAnimCtrl_.reverse().then((_) { splitAnimCtrl_.reverse(); });
                        }
                    }
                )
            )
        );
    }

    Widget leftButtonIcon() {
        return FadeTransition(
            opacity: fadeAnim_,
            child: ((iconFillFunc == null)
                ? Text("Clear")
                : buildIcon(0.0, 0.0, 0.0, Colors.black)
            )
        );
    }

    Widget centralButtonIcon() {
        return Stack(children: [
            FadeTransition(
                opacity: splitAnim_,
                child: ((iconFillFunc == null)
                    ? Text("Ghost")
                    : buildIcon(0.5, 0.5, 0.5, null)
                )
            ),
            FadeTransition(
                opacity: fadeAnim_,
                child: ((iconFillFunc == null)
                    ? Text("All But 1st")
                    : buildIcon(0.5, 0.0, 0.0, Colors.black)
                )
            )
        ]);
    }

    Widget rightButtonIcon() {
        return FadeTransition(
            opacity: fadeAnim_,
            child: ((iconFillFunc == null)
                ? Text("Keep")
                : buildIcon(1.0, 1.0, 1.0, Colors.white)
            )
        );
    }

    Widget buildIcon(double op0, double op1, double op2, Color arrowCol) {
        return Row(children: [
            Expanded(child: Container()),
            buildIconCell(iconFillFunc(0), op0, arrowCol),
            buildIconCell(iconFillFunc(1), op1, arrowCol),
            buildIconCell(iconFillFunc(2), op2, arrowCol),
            Expanded(child: Container())
        ]);
    }

    Widget buildIconCell(Widget child, double endOpacity, Color arrowCol) {
        return Container(
            width: 32.0, height: 32.0,
            decoration: BoxDecoration(color: Colors.white, border: Border.all(width: 1.0, color: Colors.black)),
            padding: EdgeInsets.all(2.0),
            child: ((child == null)
                ? null
                : Stack(children: [
                    child,
                    Container(decoration: BoxDecoration(gradient: LinearGradient(
                        begin: Alignment.topLeft, end: Alignment.bottomRight,
                        colors: [ Color(0x80ffffff), Color.fromRGBO(255, 255, 255, 1.0 - endOpacity) ],
                        stops: [ 0.4, 0.6 ]
                    ))),
                    ((endOpacity != 0.5) ? Align(child: Icon(Icons.south_east, size: 12.0, color: arrowCol)) : Container())
                ])
            )
        );
    }
}
